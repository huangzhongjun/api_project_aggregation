package com.hj.common.utils;

import com.jayway.jsonpath.JsonPath;


/**
 * 封装这个方法，是防止json格式的测试数据写丢了键值捕获异常
 * @author Administrator
 *
 */
public class JsonPathRead {
	
	/**
	 * jsonPath取值
	 * @param data
	 * 				data[0]:body
	 * 				data[1]:jsonPath
	 * 				data[2]:接口名称
	 * @return
	 * 			解析成功，返回一个String类型的值
	 */
	public static String readJson(String ...data){
		Object object = null;
		try{
			object = JsonPath.read(data[0],data[1]).toString();
			return String.valueOf(object);
		}catch(Exception e){
			if(null == object || String.valueOf(object).equals("")){
				if(data.length == 3){
					CheckPointUtils.myAssertFalse("["+data[2]+"--接口]"+"响应内容解析不到预期字段："+data[1]);
				}
				else {
					CheckPointUtils.myAssertFalse("解析不到字段："+data[0]);
				}
			}
		}
		return "-1";
	}
}
