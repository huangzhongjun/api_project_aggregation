package com.hj.common.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

public class MyLogger {

    private static final Logger logger = LoggerFactory.getLogger(MyLogger.class);

    /**
     * 接口请求数据日志
     * @param apiName
     *                  接口名称
     * @param url
     *             接口地址
     * @param requestBody
     *                  请求body
     */
    public static void myRequestLogger(String apiName,String url,String requestBody){

        logger.info("【"+apiName+"】接口--请求---->>>>> "+url+" "+requestBody);
        Reporter.log("【"+apiName+"】接口--请求---->>>>> "+url+" "+requestBody);
    }

    /**
     *接口响应数据日志
     * @param apiName
     *                  接口名称
     * @param responseBody
     *                      响应body
     */
    public static void myResponseLogger(String apiName,String responseBody,long responseTime){
        if(apiName.contains("TTS试听") || apiName.contains("获取通话录音")){
            logger.info("【"+apiName+"】接口:有响应结果了");
        }
        else{
            logger.info("【"+apiName+"】接口:响应<<<<<----- "+responseBody);
        }
        Reporter.log("【"+apiName+"】接口-响应时间:"+responseTime+" 毫秒-响应<<<<<----- "+responseBody);
    }

    public static void myResponseLogger(String loggerInfo){
        logger.info(loggerInfo);
        Reporter.log(loggerInfo);
    }

}
