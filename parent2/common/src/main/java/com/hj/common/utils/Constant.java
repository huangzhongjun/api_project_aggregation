package com.hj.common.utils;

public class Constant {

    /**SaaS慧声功能模块用例数据表**/
    public static final String SaaSShTable ="hs_test_data";
    /**SaaS慧声业务流程树用例数据表**/
    public static final String SaaSShFlowTreeTable = "hs_test_data_flow_tree";
    /**SaaS慧声前置操作用例数据表*/
    public static final String SaaSShGroup = "hs_test_data_group";
    /**SaaS慧声调试用例数据表*/
    public static final String SaaSShOrgRoleTable = "hs_test_data_org_role";
}
