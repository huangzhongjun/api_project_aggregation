package com.hj.common.dto;

import lombok.Data;

@Data
public class TestDataBean {

	/**方法名**/
	private String methodName;
	/**接口描述**/
	private String apiDescription;
	/**请求接口地址**/
	private String url;
	/**请求数据**/
	private String requestData;
	/**检查点**/
	private String checks;
	/**请求头**/
	private String head;
	/**数据关联**/
	private String dataAssociation;
	/**辅助字段**/
	private String assist;

	@Override
	public String toString() {
		return "TestDataBean{" +
				"methodName='" + methodName + '\'' +
				", apiDescription='" + apiDescription + '\'' +
				", api-url='" + url + '\'' +
				", requestData='" + requestData + '\'' +
				", checks='" + checks + '\'' +
				", head='" + head + '\'' +
				", dataAssociation='" + dataAssociation + '\'' +
				", assist='" + assist + '\'' +
				'}';
	}
}
