package com.hj.common.dto;

import lombok.Data;

@Data
public class ExecuteCount {

   private int executeCount = 1;
}
