package com.hj.common.utils;

import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class ImgToByte {

	static BASE64Encoder encoder = new BASE64Encoder();

	/**
	 * 图片转成流
	 * @param imgPath	图片路径
	 * @param imgFormat	图片格式
	 * @return
	 */

	public static String imgToByte(String imgPath, String imgFormat) {

		File file = new File(imgPath);
		
		InputStream resourceAsStream = null;
		
		try {
			resourceAsStream = new FileInputStream(file);
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}

		BufferedImage bi = null;
		try {

			bi = ImageIO.read(resourceAsStream);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bi, imgFormat, baos); // 经测试转换的图片是格式这里就什么格式，否则会失真
			byte[] bytes = baos.toByteArray();

			return encoder.encodeBuffer(bytes).trim();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			if(null != resourceAsStream){
				try {
					resourceAsStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
