package com.hj.common.base;

import com.hj.common.dto.ExecuteCount;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;


public class TestNgBase extends AbstractTestNGSpringContextTests {
	
	public Logger logger = LoggerFactory.getLogger(TestNgBase.class);

	public ExecuteCount executeCount = new ExecuteCount();

	@BeforeTest
	@Parameters({"caseName"})
	public void beforeTest(String caseName){

		logger.info("===【"+caseName+"】=================== 功  能  模  块  用  例  开  始  执  行  ！！");

	}
	
	@AfterTest
	@Parameters({"caseName"})
	public void afterTest(String caseName){
		logger.info("===【"+caseName+"】==================== 功  能  模  块  用  例  执  行  结  束  ！！\n");
		executeCount.setExecuteCount(1);
	}

}
