package com.hj.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 断言工具类
 * 
 * @author huangjun
 *
 */
public class CheckPointUtils {

	private static final Logger logger = LoggerFactory.getLogger(CheckPointUtils.class);

	/**
	 * 断言 【指定字段】和【响应code码和msg】
	 * @param resultBody
	 * 					响应报文
	 * @param caseDescribe
	 * 					测试数据对象
	 * @param map
	 * 			要指定断言的字段,map类型
	 */
	public static void checkPointsAndCodeMsg(String resultBody, String caseDescribe,Map<String,Object> map){

		checkResponseCodeMsg(resultBody, caseDescribe);
		checkPoints(resultBody,map,caseDescribe);
	}

	/**
	 * 断言指定字段
	 * @param resultBody
	 * 					响应报文
	 * @param checkMap
	 * 			要指定断言的字段,Map类型
	 * @param describe
	 * 				用例描述
	 */
	public static void checkPoints(String resultBody,Map<String,Object> checkMap,String describe) {
		List<Object> errorList = new ArrayList<>();
		List<Object> trueList = new ArrayList<>();
		for (Map.Entry<String, Object> entry : checkMap.entrySet()) {
			String value = JsonPathRead.readJson(resultBody, entry.getKey(), describe);
			Object object = entry.getValue();
			String actualValue =  String.valueOf(object);
			if (value.equalsIgnoreCase(actualValue)) {
				trueList.add(entry.getKey()+"="+actualValue);
			} else {
				errorList.add(entry.getKey()+"="+actualValue);
			}
		}
		if (errorList.size() > 0) {
			myAssertFalse("【" + describe+ "】接口--[响应body里字段断言失败]:期望值是：" + errorList.toString());
		} else {
			MyLogger.myResponseLogger("【" + describe+ "】接口--[响应body里字段断言成功]:" + trueList.toString());
		}
	}

	/**
	 * 断言响应 code 和 msg
	 * @param resultBody
	 * 					响应报文
	 * @param caseDescribe
	 * 					用例描述
	 */
	public static void checkResponseCodeMsg(String resultBody, String caseDescribe){

		String code = JsonPath.read(resultBody, "$.statusCode").toString();
		String msg = JsonPath.read(resultBody, "$.msg").toString();

		if (code.equals("200") && msg.equals("操作成功")) {

			MyLogger.myResponseLogger("【" + caseDescribe+ "】接口--[code、msg 断言正确 ]");
		}

		else{
			myAssertFalse("【" + caseDescribe+ "】接口--[code、msg 断言失败]");
		}
	}

	public static void myAssertFalse(String message){
		logger.error(message);
		Assert.fail(message);
	}
	
	/**
	 * 精确比较
	 * @param actual	实际值
	 * @param expected	期望值
	 */
	public static <T> void myAssertEquals(String caseDescribe,T actual, T expected) {
	
		if(actual.equals(expected)) {

		MyLogger.myResponseLogger("【" + caseDescribe + "】接口断言成功:" + "| 实际值:" + actual + "| 预期值:" + expected);
	}
		else {
		logger.error("【" + caseDescribe + "】接口断言失败:" + "| 实际值:" + actual + "| 预期值:" + expected);
	}
		Assert.assertEquals(actual,expected,"【"+caseDescribe+"】接口断言结果:" + "| 实际值:" + actual + "| 预期值:" + expected);
}

	/**
	 * 获取检查点数据
	 * @param keys
	 * 				每个检查点对象的键
	 * @param checkPoint
	 * 					检查点数据(json格式)
	 * @return
	 * 			返回 List<Map<String,Object>>
	 */
	public static List<Map<String,Object>> getCheckPointList(String keys,String checkPoint){

		JSONObject jsonObject = ParseType.parseStrToJsonObject(checkPoint);
		String[] split = keys.split(",");

		List<Map<String,Object>> list = new ArrayList<>();
		for (String mapKey:split) {

			Map<String,Object>  map = (Map<String, Object>) jsonObject.get(mapKey);

			list.add(map);
		}
		return  list;
	}

}
