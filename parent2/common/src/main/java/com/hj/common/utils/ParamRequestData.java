package com.hj.common.utils;

import java.util.Map;

public class ParamRequestData{

    /**
     * 对请求数据的变量进行赋值(请求参数和检查点)
     * @param variableMap
     *                  预置参数数据
     * @param dataMap
     *              请求数据
     * @return
     */
    public static Map<String, Object> paramRequestData(Map<String,Object> variableMap , Map<String,Object> dataMap){

        for (Map.Entry<String, Object> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            String value ;
            try {
                value = String.valueOf(entry.getValue());
            }catch (Exception e){
                continue;
            }
            if (value.contains("$")) {	//判断是否需要参数化
                String variableMapKey = value.substring(2,value.length()-1);
                if(variableMap.containsKey(variableMapKey)){
                    dataMap.put(key,variableMap.get(variableMapKey));
                }
            }
        }
        return dataMap;
    }
}
