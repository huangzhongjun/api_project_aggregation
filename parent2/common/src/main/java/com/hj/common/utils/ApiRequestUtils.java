package com.hj.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.hj.common.dto.TestDataBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApiRequestUtils {

	private static CloseableHttpClient httpClient;
	
	private CloseableHttpResponse response;
	
	private Pattern pattern = Pattern.compile("\\$\\{(.*?)\\}");

	/**接口上下游数据关联**/
	private Map<String, Object> saveLinkedMap = new ConcurrentHashMap<>();

	/**头信息里的认证数据关联**/
    private Map<String, Object> saveHeadInfoMap = new ConcurrentHashMap<>();
	
	private String responseBody;
	
	static {
		PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
		manager.setMaxTotal(200); //连接池最大并发连接数
		manager.setDefaultMaxPerRoute(200);//单路由最大并发数,路由是对maxTotal的细分
		httpClient = HttpClients.custom().setConnectionManager(manager).build();

	}

	private static RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10000).setConnectTimeout(5000) .setConnectionRequestTimeout(5000).build();
	/**
	 * post-json格式请求参数的 请求
	 * @param dateBean
	 * @return
	 * 			返回 CloseableHttpResponse 类型的响应报文
	 */
	public CloseableHttpResponse postRequestForJsonToResponse(TestDataBean dateBean){
		requestPublicPart(dateBean);
		HttpPost httpPost = entityPostJsonData(dateBean.getRequestData(),dateBean.getUrl());
		HttpRequestBase httpBase = setHead(httpPost,dateBean.getHead());
		try {
			httpBase.setConfig(config);
			long startTimeInMillis = System.currentTimeMillis();
			response = httpClient.execute(httpBase);
			long endTimeInMillis = System.currentTimeMillis();
			MyLogger.myResponseLogger("【"+dateBean.getApiDescription()+"】接口响应时间："+(endTimeInMillis-startTimeInMillis)+"s");
		}
		catch (Exception e){
			e.printStackTrace();
			Reporter.log("【"+dateBean.getApiDescription()+"】接口请求超时了");
			CheckPointUtils.myAssertFalse("【"+ dateBean.getApiDescription() + "】接口请求超时了");
		}
		return response;
	}

	/**get请求**/
	public String getRequest(TestDataBean dateBean){

		requestPublicPart(dateBean);
		HttpGet httpGet = doGet(dateBean.getRequestData(),dateBean.getUrl());
		setHead(httpGet,dateBean.getHead());
		return executeRequest(httpGet,dateBean.getApiDescription(),dateBean.getDataAssociation());
	}

	/**delete请求**/
	public String deleteRequest(TestDataBean dateBean){

		requestPublicPart(dateBean);
		HttpDelete httpDelete = doDelete(dateBean.getRequestData(),dateBean.getUrl());
		setHead(httpDelete,dateBean.getHead());
		return executeRequest(httpDelete,dateBean.getApiDescription(),dateBean.getDataAssociation());
	}

	/**post-json格式请求参数的 请求**/
	public String postRequestForJson(TestDataBean dateBean){

		requestPublicPart(dateBean);
		HttpPost httpPost = entityPostJsonData(dateBean.getRequestData(),dateBean.getUrl());
		return postProcess(httpPost,dateBean.getApiDescription(),dateBean.getDataAssociation(),dateBean.getHead());
	}
	
	/**post-键值对格式请求参数 请求**/
	public String postRequestKeyValuePair(TestDataBean dateBean){

		requestPublicPart(dateBean);
		HttpPost httpPost = doPostKeyValue(dateBean.getRequestData(),dateBean.getUrl());
		return postProcess(httpPost,dateBean.getApiDescription(),dateBean.getDataAssociation(),dateBean.getHead());
	}
	
	/**文件上传 请求**/
	public String fileUpLoad(TestDataBean dateBean){
		
		initAddressAndRequestParam(dateBean);
		HttpPost httpPost = doUpload((Map)JSONObject.parseObject(dateBean.getRequestData()),dateBean.getUrl());
		return postProcess(httpPost,dateBean.getApiDescription(),dateBean.getDataAssociation(),dateBean.getHead());
	}

	/**
	 * 请求操作的公共部分：ip+uir 、请求参数关联数据的赋值、日志打印
	 * @param dateBean
	 */
	private void requestPublicPart(TestDataBean dateBean){

		initAddressAndRequestParam(dateBean);
		MyLogger.myRequestLogger(dateBean.getApiDescription(),dateBean.getUrl(),dateBean.getRequestData());
	}
	
	/**处理post请求的头信息**/
	private String postProcess(HttpPost httpPost,String apiDescribe,String relevancy,String head){
		
		HttpPost post = (HttpPost) setHead(httpPost, head);
		return executeRequest(post,apiDescribe,relevancy);
	}
	
	/**拼接ip和url和初始化请求参数里的关联参数**/
	private void initAddressAndRequestParam(TestDataBean dateBean){
		if(dateBean.getApiDescription().equals("登录") || dateBean.getUrl().substring(1,7).equals("notify")
				|| dateBean.getUrl().substring(1,4).equals("uac")){
			dateBean.setUrl(System.getProperty("pip") + dateBean.getUrl());
		}
		else{
			dateBean.setUrl(System.getProperty("ip") + dateBean.getUrl());
		}
		String newRequestParam = patternLinked(dateBean.getRequestData(), dateBean.getApiDescription());
		dateBean.setRequestData(newRequestParam);
		String checks = patternLinked(dateBean.getChecks(), dateBean.getApiDescription());
		dateBean.setChecks(checks);
	}

	/**
	 * 设置头基本信息
	 * @param httpBase
	 * @return
	 */
	private HttpRequestBase setHead(HttpRequestBase httpBase,String head){

		httpBase.setHeader(HTTP.CONTENT_ENCODING,"UTF-8");

		if(null != saveHeadInfoMap){

			for (Entry<String, Object> headMap : saveHeadInfoMap.entrySet()) {
				httpBase.setHeader(headMap.getKey(), (String) headMap.getValue());
			}
		}

		Map<String, Object> headMap = parseMap(head);
		if(null != headMap){
			for(Entry<String ,Object > map : headMap.entrySet())
			httpBase.setHeader(map.getKey(),map.getValue().toString());
		}
		return httpBase;
	}

	/**
	 * 执行请求
	 * @param httpBase
	 * @param apiName	用例名称
	 * @param relevancy	关联
	 * @return	返回响应内容的Json格式类型
	 */
	private String executeRequest(HttpRequestBase httpBase,String apiName,String relevancy){

		try {
			httpBase.setConfig(config);
			long startTimeInMillis = System.currentTimeMillis();
			response = httpClient.execute(httpBase);
			long endTimeInMillis = System.currentTimeMillis();
			loginAuthorization(apiName);
			responseBody=ParseType.parseRespBodyToString(response);
			MyLogger.myResponseLogger(apiName,responseBody,(endTimeInMillis-startTimeInMillis));
			checkResponseCode(response,apiName);

		} catch (Exception e) {
			Reporter.log("【"+apiName+"】接口请求超时了");
			CheckPointUtils.myAssertFalse("【"+ apiName + "】接口请求超时了");
		}finally{
			if(null != response){
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		saveLinked(responseBody, relevancy,apiName);
		return responseBody;
	}

	/**uac登录和机构管理角色用户登录的凭证**/
	private void loginAuthorization(String apiName){
		if(apiName.contains("登录")){
			//获取头信息里的登录认证
			Header[] authorizations = response.getHeaders("Authorization");
			String key = authorizations[0].toString().split(":")[0].trim();
			String value = authorizations[0].toString().split(":")[1].trim();
			saveHeadInfoMap.put(key,value);
		}
	}

	/**
	 * 处理get请求参数
	 * @param requestParam
	 * @param url
	 * @return  HttpGet类型 对象
	 */
	private HttpGet doGet(String requestParam,String url){

		try{
			Map<String, Object> map = parseMap(requestParam);
			UrlEncodedFormEntity entity = setEntity(map);
			return new HttpGet(url + "?" + EntityUtils.toString(entity, "UTF-8"));
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private HttpDelete doDelete(String requestParam,String url){

		try{
			Map<String, Object> map = parseMap(requestParam);
			UrlEncodedFormEntity entity = setEntity(map);
			return new HttpDelete(url + "?" + EntityUtils.toString(entity, "UTF-8"));
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 请求数据实体化
	 * @param requestParam
	 * @param url
	 * @return HttpPost类型 对象
	 */
	private HttpPost entityPostJsonData(String requestParam,String url){
		StringEntity entity = new StringEntity(requestParam, Charset.forName("UTF-8"));
		entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		entity.setContentType("application/json");
		HttpPost post = new HttpPost(url);
		post.setEntity(entity);
		return post;
	}



	/**
	 * 处理post请求 参数 <键值对格式参数>
	 * @param requestParam
	 * @param url
	 * @return  HttpPost类型 对象
	 */
	private HttpPost doPostKeyValue(String requestParam,String url){

		HttpPost post = new HttpPost(url);

		Map<String, Object> map = parseMap(requestParam);

		UrlEncodedFormEntity entity ;

		try {
			entity = setEntity(map);
			post.setEntity(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return post;
	}

	/**
	 * 处理 文件上传
	 * @param requestDataMap
	 * @param url
	 * @return
	 */
	private HttpPost doUpload(Map<String,String> requestDataMap,String url){
		HttpPost post = new HttpPost(url);
		MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
		multipartEntityBuilder.setCharset(Charset.forName("UTF-8"));//设置请求的编码格式
		multipartEntityBuilder.addBinaryBody("file",new File(requestDataMap.get("file")));
		requestDataMap.remove("file",requestDataMap.get("file"));
		if(requestDataMap.size() != 0){
			for(Entry<String,String> map : requestDataMap.entrySet()){
				multipartEntityBuilder.addTextBody(map.getKey(),map.getValue());
			}
		}
		HttpEntity build = multipartEntityBuilder.build();
		post.setEntity(build);
		return post;
	}

	/** 键值对请求参数解析成map **/
	private Map<String, Object> parseMap(String params) {
		
		Map<String, Object> map = null;
		if(StringUtils.isNotBlank(params) && !"null".equalsIgnoreCase(params)){
			
			map = new HashMap<>();
			String nameValue[] = params.split(",");
			for (String s : nameValue) {
				String value[] = s.split("=");
				map.put(value[0], value[1]);
			}
		}
		return map;	
	}

	/** form表单的map类型参数解析成实体 **/
	private UrlEncodedFormEntity setEntity(Map<String, Object> map) throws Exception {
		
		List<BasicNameValuePair> formList = new ArrayList<BasicNameValuePair>();
		
		for (Entry<String, Object> entry : map.entrySet()) {
			try {
				formList.add(new BasicNameValuePair(entry.getKey(), (String) entry.getValue()));
			} catch (Exception e) {
				CheckPointUtils.myAssertFalse("参数解析错误，可能是没有参数");
			}
			
			
		}
		return new UrlEncodedFormEntity(formList);
	}

	
	
	/** 断言接口请求是否成功-状态码200--302或者304则要重定向 **/
	private String checkResponseCode(CloseableHttpResponse response,String describe) {
		
		if(!describe.contains("有误") && !describe.contains("异常")){
		
			if (response.getStatusLine().getStatusCode() != 200) {
				
				if(response.getStatusLine().getStatusCode()==301 || response.getStatusLine().getStatusCode()==302){
					
					return getRequestR(describe,httpClient, getLocation(response));
				}
				else{
					String responseBody = ParseType.parseRespBodyToString(response);
	
					CheckPointUtils.myAssertFalse("【"+describe+"】接口--服务响应状态码错误非200 ：<<--："+responseBody);
				}
			}
			else{
				
				return "1";
			}
		}
		return null;
	}

	/** get重定向的请求 **/
	private String getRequestR(String describe,CloseableHttpClient httpClient, String apiUrl) {

		if (!apiUrl.startsWith("http")) {
			apiUrl = "http://" + apiUrl;
		}
		HttpGet get = new HttpGet(apiUrl);
		try {
			response = httpClient.execute(get);
			checkResponseCode(response,describe);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isRespNull(response);
	}

	/**
	 * 获取Post请求中返回的Location--重定向的url地址
	 * 
	 * @param response
	 * @return
	 */
	private String getLocation(HttpResponse response) {

		return response.getFirstHeader("Location").getValue();
	}

	/**
	 * 判断接口响应内容是否为空
	 * 
	 * @param response
	 * @return 返回json
	 */
	private String isRespNull(CloseableHttpResponse response) {
		
		try {
			if (null != response) {
				
				return ParseType.parseRespBodyToString(response);
			} 
			else {
				CheckPointUtils.myAssertFalse("接口响应内容为空-------------------");
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**获取关联数据的map**/
	public Map<String, Object> getSaveLinked(){

		return saveLinkedMap;
	}

	/**获取头信息数据的map**/
	public Map<String, Object> getSaveHeadInfo(){

		return saveHeadInfoMap;
	}
	
	/**
	 * 保存存关联的数据至Map<用于后面的接口请求参数所用到的参数值>
	 * @param body	响应内容
	 * @param relevancy	关联
	 * @param describe 用例描述
	 * @return	Map
	 */
	public Map<String, Object> saveLinked(String body, String relevancy,String describe) {
		if(StringUtils.isNotBlank(relevancy) && !"null".equalsIgnoreCase(relevancy)){
			Map<String, Object> map = parseMap(relevancy);
			for (Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				String jp = entry.getValue().toString();
				saveLinkedMap.put(key, JsonPathRead.readJson(body,jp,describe));
/*				if (key.endsWith("*")) {	//判断获取响应内容的字段个数>1
					String before = key.split("_")[0];
					String string = entry.getValue().toString();
					List<String> list = JsonPath.read(body, string);
					for (int i = 0; i < list.size(); i++) {
						System.out.println("-----------" + before + "_" + (i + 1) + "-------:----------" + list.get(i));
						saveLinkedMap.put(before + "_" + (i + 1), list.get(i));
					}
				} else {
				}*/
			}
			return saveLinkedMap;
		}
		return saveLinkedMap;
	}

	/**
	 * 检查请求参数是否有匹配到规则(需要用到上一个接口的返回内容的字段值)
	 * @param requestData
	 */
	private String patternLinked(String requestData,String apiDescription) {
		if(!StringUtils.isNotBlank(requestData)){
			return requestData;
		}
		String newValue;
		Matcher matcher = pattern.matcher(requestData);
		while (matcher.find()) {
			try{
				String values = saveLinkedMap.get(matcher.group(1)).toString();
				newValue = requestData.replace(matcher.group(), values);
				requestData = newValue;
			}catch(Exception e){
				MyLogger.myResponseLogger("【"+apiDescription+"】接口-紧急情况-saveLinkedMap里没有找到:"+matcher.group(1)+" 的键值");
			}
		}
		return requestData;
	}
}
