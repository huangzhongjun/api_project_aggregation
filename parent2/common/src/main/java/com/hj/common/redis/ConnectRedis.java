package com.hj.common.redis;

import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;

public class ConnectRedis {
	
	/**set单个key值**/
	public static void setKeyValue(String key,String value){
		Jedis jedis = RedisPool.getInstence();
		jedis.set(key, value);
//		RedisPool.closeRedisResouce(jedis);
	}
	/**get单个key值**/
	public static String getKeyValue(String key){
		Jedis jedis = RedisPool.getInstence();
		String value= jedis.get(key);
	//	RedisPool.closeRedisResouce(jedis);
		return value;
	}

	/**set一个map集合key值**/
	public static void setMapValue(String key,Map<String ,String> map){
		Jedis jedis = RedisPool.getInstence();
		jedis.hmset(key,map);
	//	RedisPool.closeRedisResouce(jedis);
	}

	/**get一个map集合key值**/
	public static List<String> getMapValue(String key){
		Jedis jedis = RedisPool.getInstence();
		List<String> list=jedis.hvals("key");
	//	RedisPool.closeRedisResouce(jedis);
		return list;
	}

	/**get一个map集合key 的某一个key的值**/
	public static List<String> getMapKyeValue(String key,String c_key){
		Jedis jedis = RedisPool.getInstence();
		List<String> list=jedis.hmget(key,c_key);
	//	RedisPool.closeRedisResouce(jedis);
		return list;

	}

	/**set一个list集合的key值**/
	public static void setListValue(String key,List<String> list){
		Jedis jedis = RedisPool.getInstence();
		for(int i=0;i<list.size();i++){
			 jedis.lpush(key,list.get(i));
		}
	//	RedisPool.closeRedisResouce(jedis);
	}
	/**get list集合的全部值**/
	public static void getListValue(){
		Jedis jedis = RedisPool.getInstence();
		jedis.lrange("listDemo", 0, -1);
//		jedis.lrange("listDemo", 0, 1);
	}
}
