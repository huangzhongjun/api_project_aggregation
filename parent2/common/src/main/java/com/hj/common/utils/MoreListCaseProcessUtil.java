package com.hj.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理很多的有序的用例
 */
public class MoreListCaseProcessUtil {

    /**处理很多的有序的用例**/
    public static Integer[]  moreListCaseProcess(int startCaseId,int endCaseId){
        List<Integer> list = new ArrayList<>();
        for(int i = startCaseId; i<=endCaseId; i++){
            list.add(i);
        }
        Integer caseId[] = new Integer[list.size()];

        Integer[] caseIds = list.toArray(caseId);
        return  caseIds;
    }
}
