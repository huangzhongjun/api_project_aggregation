package com.hj.common.utils;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**删除日志文件**/
public class DeleteFile {
	private static final Logger logger = LoggerFactory.getLogger(DeleteFile.class);

	public static void deleteGoalFile(){
		try {
			FileUtils.deleteDirectory(new File(System.getProperty("user.dir")+File.separator+"extentReport"+File.separator+"logs"));
			logger.info("日志文件删除成功");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
