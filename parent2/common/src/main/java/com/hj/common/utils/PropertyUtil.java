package com.hj.common.utils;

import java.io.*;
import java.util.Map;
import java.util.Properties;


public class PropertyUtil {
	
	private Properties properties = new Properties();
	private String filePath;

	public PropertyUtil(String file) {
		
		filePath = file;

		properties = loadProperty();
	}

	/**
	 * 
	 * @param file
	 * @param n
	 *            随便传个值，只做构造器的区别
	 */
	public PropertyUtil(String file, int n) {

		filePath = file;
		
		properties = loadPropertyRoot();
	}
	
	public Properties getProperties(){
		
		return properties;
	}
	

	/**
	 * 获取配置文件的某个键对应的值,找不到直接报错
	 * 
	 * @param key
	 * @return
	 */
	public String getproperValue(String key) {
		String value = null;
		if (properties.containsKey(key)) {
			try {
				value = properties.getProperty(key).trim();
			} catch (Exception e) {
				e.printStackTrace();
				CheckPointUtils.myAssertFalse(e.getMessage());
			}
		} else {
			CheckPointUtils.myAssertFalse("没有找到相应的键:[" + key + "] 请检查配置文件或测试数据");
		}
		return value;
	}
	
	/**判断属性文件里是否包含key,包含则返回true,否在返回false
	 * @param key
	 * @return
	 */
	public boolean isContainsKey(String key) {

		if (properties.containsKey(key)) {
			
			return true;
			
		} else {
			
			return false;
		}
	}

	/**
	 * 自动生产属性文件及内容
	 * 
	 * @param propertyMap
	 */
	public void setPropertyValue(Map<String, String> propertyMap) {

		FileOutputStream out = null;

		try {
			for (Map.Entry<String, String> entry : propertyMap.entrySet()) {
				out = new FileOutputStream(this.filePath);
				setProValue(entry.getKey(), entry.getValue(), out);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void setProValue(String key, String value, OutputStream fos) {

		// if(properties.containsKey(key)){
		try {
			properties.setProperty(key, value);
			properties.store(new OutputStreamWriter(fos, "UTF-8"), "Update  value");
		} catch (Exception e) {
			e.printStackTrace();

		}
		// }else{
		// MyAssert.myAssertFalse("没有找到相应的键:["+key+"] 请检查配置文件或测试数据");
		// }
	}

	/**
	 * 加载属性配置文件
	 * 
	 * @return
	 */
	private Properties loadProperty() {

		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream resourceAsStream = contextClassLoader.getResourceAsStream(filePath);

		BufferedReader bf = null;

		try {
			
			bf = new BufferedReader(new InputStreamReader(resourceAsStream, "UTF-8"));
			
			properties.load(bf);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != bf) {

				try {
					bf.close();

				} catch (IOException e) {

					e.printStackTrace();
				}
			}
			if (null != resourceAsStream) {

				try {

					resourceAsStream.close();

				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		}
		return properties;
	}

	/**
	 * 加载属性配置文件
	 * 
	 * @return
	 */
	private Properties loadPropertyRoot() {

		BufferedReader bf = null;

		File file = new File(this.filePath);
		try {
			if(!file.exists()){
				file.createNewFile();
			}
			bf = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
			properties.load(bf);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != bf) {

				try {
					bf.close();

				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		}
		return properties;
	}
}
