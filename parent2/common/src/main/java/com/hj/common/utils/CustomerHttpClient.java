package com.hj.common.utils;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 此类只供参考
 * @author Administrator
 *
 */
public class CustomerHttpClient {

	private static final Logger log = LoggerFactory.getLogger(CustomerHttpClient.class);

	//	private static final String CHARSET = "UTF-8";
	private static CloseableHttpClient customerHttpClient;

	/**
	 * 连接超时时间 可以配到配置文件 （单位毫秒）
	 */
	private static int MAX_TIME_OUT = 10000;

	// 设置整个连接池最大连接数
	private static int MAX_CONN = 200;
	// 设置单个路由默认连接数
	private static int SINGLE_ROUTE_MAX_CONN = 100;

	// 连接丢失后,重试次数
	private static int MAX_EXECUT_COUNT = 0;
	// 创建连接管理器
	private static PoolingHttpClientConnectionManager connManager = null;

	private CustomerHttpClient() {
	}

	public static synchronized CloseableHttpClient getHttpClient() {
		try {
			if (null == customerHttpClient) {

				// 创建SSLSocketFactory
				// 定义socket工厂类 指定协议（Http、Https）
				Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
						.register("http", PlainConnectionSocketFactory.getSocketFactory())
						.register("https", createSSLConnSocketFactory())// SSLConnectionSocketFactory.getSocketFactory()
						.build();

				// 创建连接管理器
				connManager = new PoolingHttpClientConnectionManager(registry);
				connManager.setMaxTotal(MAX_CONN);// 设置最大连接数
				connManager.setDefaultMaxPerRoute(SINGLE_ROUTE_MAX_CONN);// 设置每个路由默认连接数

				// 设置目标主机的连接数
				// HttpHost host = new HttpHost("account.dafy.service");//针对的主机
				// connManager.setMaxPerRoute(new HttpRoute(host),50);
				// 每个路由器对每个服务器允许最大50个并发访问

				// 创建httpClient对象
				customerHttpClient = HttpClients.custom().setConnectionManager(connManager)
						.setRetryHandler(httpRequestRetry()).setDefaultRequestConfig(config()).build();

			}
		} catch (Exception e) {
			log.error("获取httpClient(https)对象池异常:" + e.getMessage(), e);
		}
		return customerHttpClient;
	}

	/**
	 * 创建SSL连接
	 * 
	 * @throws Exception
	 */
	private static SSLConnectionSocketFactory createSSLConnSocketFactory() throws Exception {
		// 创建TrustManager
		X509TrustManager xtm = new X509TrustManager() {
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};
		// TLS1.0与SSL3.0基本上没有太大的差别，可粗略理解为TLS是SSL的继承者，但它们使用的是相同的SSLContext
		SSLContext ctx = SSLContext.getInstance("TLS");
		// 使用TrustManager来初始化该上下文，TrustManager只是被SSL的Socket所使用
		ctx.init(null, new TrustManager[] { xtm }, null);
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(ctx);
		return sslsf;
	}

	/**
	 * 配置请求连接重试机制
	 */
	private static HttpRequestRetryHandler httpRequestRetry() {
		HttpRequestRetryHandler httpRequestRetryHandler = new HttpRequestRetryHandler() {
			public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
				if (executionCount >= MAX_EXECUT_COUNT) {// 如果已经重试MAX_EXECUT_COUNT次，就放弃
					return false;
				}
				if (exception instanceof NoHttpResponseException) {// 如果服务器丢掉了连接，那么就重试
					// logger.error("httpclient 服务器连接丢失");
					return true;
				}
				if (exception instanceof SSLHandshakeException) {// 不要重试SSL握手异常
					// logger.error("httpclient SSL握手异常");
					return false;
				}
				if (exception instanceof InterruptedIOException) {// 超时
					// logger.error("httpclient 连接超时");
					return false;
				}
				if (exception instanceof UnknownHostException) {// 目标服务器不可达
					// logger.error("httpclient 目标服务器不可达");
					return false;
				}
				if (exception instanceof ConnectTimeoutException) {// 连接被拒绝
					// logger.error("httpclient 连接被拒绝");
					return false;
				}
				if (exception instanceof SSLException) {// ssl握手异常
					// logger.error("httpclient SSLException");
					return false;
				}

//				HttpClientContext clientContext = HttpClientContext.adapt(context);
//				HttpRequest request = clientContext.getRequest();
				// 如果请求是幂等的，就再次尝试 暂时没理解先注释
				// if (!(request instanceof HttpEntityEnclosingRequest)) {
				// return true;
				// }
				return false;
			}

		};
		return httpRequestRetryHandler;
	}

	/**
	 * 配置默认请求参数
	 */
	private static RequestConfig config() {
		// 配置请求的超时设置 其他参数可以追加
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(MAX_TIME_OUT)// 设置从连接池获取连接实例的超时
				.setConnectTimeout(MAX_TIME_OUT)// 设置连接超时
				.setSocketTimeout(MAX_TIME_OUT)// 设置读取超时
				.build();
		return requestConfig;
	}
	
	/**
	 * 设置请求头cookie信息
	 * 
	 * @param headers
	 * @param request
	 * @return
	 */
	/*	private static HttpRequest setHeaders(Map<String, Object> headers, HttpRequest request) {
		for (Map.Entry entry : headers.entrySet()) {
			if (!entry.getKey().equals("Cookie")) {
				request.addHeader((String) entry.getKey(), (String) entry.getValue());
			} else {
				Map<String, Object> Cookies = (Map<String, Object>) entry.getValue();
				for (Map.Entry entry1 : Cookies.entrySet()) {
					request.addHeader(new BasicHeader("Cookie", (String) entry1.getValue()));
				}
			}
		}
		return request;
	}*/

	/**
	 * 获取所有响应头信息转成map格式
	 * @param url
	 * @return
	 */
	public static Map<String, String> getCookie(String url) {
		CloseableHttpClient httpClient = getHttpClient();
		HttpRequest httpGet = new HttpGet(url);
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute((HttpGet) httpGet);
			Header[] headers = response.getAllHeaders();
			Map<String, String> cookies = new ConcurrentHashMap<String, String>();
			for (Header header : headers) {
				cookies.put(header.getName(), header.getValue());
			}
			return cookies;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}
}
