package com.hj.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;


public class CreatXmlFile {

	private static final Logger logger = LoggerFactory.getLogger(CreatXmlFile.class);

	private PropertyUtil property =null;
	
	private String casePackageName = null;
	
	private String listenerPackageName = null;
	
	public CreatXmlFile(String casePackageName,String listenerPackageName){
		
		this.casePackageName = casePackageName;
		this.listenerPackageName = listenerPackageName;
	}
	
	/**
	 * 自动创建testng.xml文件，生成可执行用例
	 * @param className
	 * 					项目包名.类名
	 */
	public void creatTestNGXml(String className) {
		
		property = getProjectCaseFile(className);

		Document doc = DocumentHelper.createDocument();
		Element root = DocumentHelper.createElement("suite");
		doc.setRootElement(root);
		root.addAttribute("name", "SaaS系统");
		root.addAttribute("preserve-order", "true");
		root.addAttribute("parallel", "tests");
		root.addAttribute("thread-count","1");// String.valueOf(className.length)
		
		Element listenersNode = root.addElement("listeners");
		
		Element myListenerNode = listenersNode.addElement("listener");
		myListenerNode.addAttribute("class-name", this.listenerPackageName);
		
//		Element retryListenerNode = listenersNode.addElement("listener");
//		retryListenerNode.addAttribute("class-name", "cn.itcast.util.RetryListener");
		
		Element HTMLReporter = listenersNode.addElement("listener");
		HTMLReporter.addAttribute("class-name", "org.uncommons.reportng.HTMLReporter");
		Element JUnitXMLReporter = listenersNode.addElement("listener");
		JUnitXMLReporter.addAttribute("class-name", "org.uncommons.reportng.JUnitXMLReporter");
		
		//自定义执行某些测试用例
		if(StringUtils.isNotBlank(System.getProperty("customCase"))){
			
			customCase(root,System.getProperty("customCase"),className);
		}
		//执行全部用例
		else{
			generateAllCase(root,className);
		}

		OutputFormat format = new OutputFormat("    ", true);
		format.setEncoding("UTF-8");
		XMLWriter xw = null;
		try {
			xw = new XMLWriter(new FileOutputStream(new File("testNg-debug.xml")),format);
			xw.write(doc);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (xw != null) {
				try {
					xw.close();
					logger.info("testNg.xml文件创建成功");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	 private void customCase(Element root,String methodKeys,String className){
		
		String methodKeysArray[] = methodKeys.trim().split(",");

		for (int i = 0; i < methodKeysArray.length; i++) {
		
		String testMethods = property.getproperValue(methodKeysArray[i].toString()).trim();
		String testMethodsArray[] = testMethods.split(",");

		Element test = root.addElement("test");
		test.addAttribute("name",property.getproperValue(methodKeysArray[i]+".Name"));
		test.addAttribute("preserve-order","true");
		
		Element parameterNode = test.addElement("parameter");
		parameterNode.addAttribute("name","modulesCase");
		parameterNode.addAttribute("value",property.getproperValue(methodKeysArray[i]+".Name"));

		generateCase(test,testMethodsArray,className);
		}
	 }
	
	public void generateAllCase(Element root,String className){
		
		Properties properties = property.getProperties();
		
		Iterator<Entry<Object, Object>> iterator = properties.entrySet().iterator();
		
		while(iterator.hasNext()){
			
			Entry<Object, Object> testMethods = iterator.next();
			
			if(!testMethods.getKey().toString().contains(".Name")){
				
				String testMethodsArray[] = testMethods.getValue().toString().split(",");

				Element test = root.addElement("test");
				test.addAttribute("name",property.getproperValue(testMethods.getKey()+".Name"));
				test.addAttribute("preserve-order","true");
				
				Element parameterNode = test.addElement("parameter");
				parameterNode.addAttribute("name","modulesCase");
				parameterNode.addAttribute("value",property.getproperValue(testMethods.getKey()+".Name"));

				generateCase(test,testMethodsArray,className);
			}
		}
	}

	private void generateCase(Element test,String testMethodsArray[],String className){

		Element classes = test.addElement("classes");
		Element classNodes = classes.addElement("class");

		classNodes.addAttribute("name", this.casePackageName+className);
		Element methodsNode = classNodes.addElement("methods");

		for(int j = 0;j<testMethodsArray.length;j++){
			Element include = methodsNode.addElement("include");
			include.addAttribute("name", testMethodsArray[j].trim());
		}
	}

	private PropertyUtil getProjectCaseFile(String className){

		switch (className) {

			case "SaaSShVoiceCases":
				return new PropertyUtil("xx");

			case "InboundCases":
				return new PropertyUtil("xxx");
			default:
				return null;
		}
	}
}