package com.hj.common.utils;


import java.io.File;

public class PlatformConstants {

	static String classPath = System.getProperty("user.dir")+File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator;

///////////////////////////////接口地址文件路径////////////////////////////////////////////////////
	
	/**SaaS-慧声语音接口地址**/
	public static final String SaaS_SH_VOICE_API_URL = "api-url"+File.separator+"SaaS-sh-voice-api.properties";

	/**客户模板**/
	public static final String CUSTOMER_TEMPLATE = classPath+"files"+File.separator+"custImportTemplate.xlsx";
	/**录音文件**/
	public static final String RECORDING_FILE = classPath+"files"+File.separator+"A2_0.wav";
	/**短信任务里的客户信息文件**/
	public static final String CUSTOMER_INFO = classPath+"files"+File.separator+"customer.xlsx";
	

	
	

}
