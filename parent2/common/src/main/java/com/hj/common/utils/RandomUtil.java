package com.hj.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class RandomUtil {
	
	static final String  strPool="天,地,玄,黄,宇,宙,洪,荒,日,月,盈,辰,宿,列,张,寒,来,暑,往,秋,收,冬,藏,闰,馀,成,岁,律,吕,调,阳,云,腾,致,雨,露,结,为,霜,金,生,丽,水,玉,出,昆,冈,剑,号,巨,阙,珠,称,夜,光,果,珍,李,柰,菜,重,芥,姜,海,咸,河,淡,鳞,潜,羽,翔,龙,师,火,帝,鸟,官,人,皇,始,制,文,字,乃,服,衣,裳,推,位,让,国,有,虞,陶,唐,吊,民,伐,罪,周,发,殷,汤,坐,朝,问,道,垂,拱,平,章,爱,育,黎,首,臣,伏,戎,羌,遐,迩,一,体,率,宾,归,王,鸣,凤,在,竹,白,驹,食,场,化,被,草,木,赖,及,万,方"; 
	
	static final String numberPool = "1,2,3,4,5,6,7,8,9,0";
	
	static final String charsPool = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,Q,W,E,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M";

	static final String sexPool = "先生,女士";

	public static String randomSex(){
        String arrayStr[] = sexPool.split(",");
		int strLen=arrayStr.length;
		String finalRandom=random(1,strLen,arrayStr);
		return finalRandom;
	}

	/**
	 * 返回一个随机数---日期后追加7个随机数
	 * @param len 生成随机数的长度
	 * @return
	 */
	public static String random(int len){
		Date date = new Date();
		SimpleDateFormat format =new SimpleDateFormat("yyyyMMddHHmmss");
		String random=format.format(date)+randomNum(len);
		return random;
	}
	
	/**
	 * 随便生产字符池里的【字母】所连接起来的字符串
	 * @param len	生产的字符串长度
	 * @return
	 */
	public static String randomChar(int len){

		String arrayStr[]=charsPool.split(",");
		int strLen=arrayStr.length;
		String finalRandom=random(len,strLen,arrayStr);
		return finalRandom;
	}
	
	
	/**
	 * 随便生产字符池里的【数 字】所连接起来的字符串
	 * @param len	生产的字符串长度
	 * @return
	 */
	public static String randomNum(int len){

		String arrayStr[]=numberPool.split(",");
		int strLen=arrayStr.length;
		String finalRandom=random(len,strLen,arrayStr);
		return finalRandom;
	}
	
	/**
	 * 随便生产【文 字】所连接起来的字符串
	 * @param len	生产的字符串长度
	 * @return
	 */
	public static String randomStr(int len){

		String arrayStr[]=strPool.split(",");
		int strLen=arrayStr.length;
		String finalRandom=random(len,strLen,arrayStr);
		return finalRandom;
	}

	/**
	 * 随便生产文字与数字所连接起来的字符串
	 * @param strLen
	 * @param numLen
	 * @return
	 */
	public static String randomStrAndNum(int strLen,int numLen){

		String arrayStr[]=strPool.split(",");
		int strLens=arrayStr.length;
		String arrayNum[]=numberPool.split(",");
		int numLens=arrayNum.length;
		String finalRandom=random(strLen,strLens,arrayStr)+random(numLen,numLens,arrayNum);
		return finalRandom;
	}
	
	private static String random(int len,int arrayLength,String array[]){
		int i;
		int con=0;
		Random random=new Random();
		StringBuffer stringBuffer=new StringBuffer();
		while(con<len){
			i=random.nextInt(arrayLength);
			stringBuffer.append(array[i]);
			con++;
		}
		return stringBuffer.toString();
	}
}
