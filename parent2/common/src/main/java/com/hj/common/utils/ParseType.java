package com.hj.common.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import java.nio.charset.Charset;
import com.hj.common.dto.*;

/**
 * 类型解析
 * @author Administrator
 *
 */
public class ParseType {
	
	/**
	 * HttpResponse 类型的响应内容解析成字符串类型,每个方法内只能使用一次，否则会报io异常,因为EntityUtils.toString
	 * @param responseBody
	 * @return
	 */
	public static String parseRespBodyToString(CloseableHttpResponse responseBody){
		
		String stringBody ;
		try {
			stringBody = EntityUtils.toString(responseBody.getEntity(),Charset.forName("UTF-8"));
			return stringBody;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * TestDataBean 类型对象的里json数据解析
	 * @param testDataBean
	 * 					TestDataBean类型对象
	 * @return
	 */
	public static JSONObject parseStrToJsonObject(TestDataBean testDataBean){
		
		try {
			
			return JSONObject.parseObject(testDataBean.getRequestData());
			
		} catch (Exception e) {
			e.printStackTrace();
			CheckPointUtils.myAssertFalse("["+testDataBean.getApiDescription()+"]用例的数据-json格式有问题");
		}
		return null;
		
	}
	
	/**
	 * String 类型对象的里json数据解析
	 * @param strJson
	 * 				字符串
	 * @return
	 */
	public static JSONObject parseStrToJsonObject(String strJson){
		
		try {
			
			return JSONObject.parseObject(strJson);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			CheckPointUtils.myAssertFalse("json数据格式有问题");
		}
		return null;
	}
}
