package com.hj.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期时间类
 */
public class DateTimeUtil {

    /**
     * 在当时间时间上加五分钟
     * @param dateTime  年月日(YYYY-MM-dd )
     * @param minute(分钟数)
     * @return
     */
    public  static String timeAddMinute(String dateTime,int minute) {
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, minute);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateTime+"HH:mm:ss");
        String time = simpleDateFormat.format(nowTime.getTime());
        return time;
    }


    /**
     * 日期处理
     * @param dateTime	日期
     * @param Month		月份往前或往后的份数 ,正数是往后推,负数是往前推
     * @return
     */

    public static String formatDateProcess(String dateTime,int Month){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = simpleDateFormat.parse(dateTime);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, Month);
            Date time = calendar.getTime();
            return simpleDateFormat.format(time);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 时间延迟(分钟级别)
     * @return
     */
/*    public static String timeLateProcess(String dateTime){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateTime+"HH:mm:ss");
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MINUTE, 20);//当前时间推迟20分钟
            Date time = calendar.getTime();
            return simpleDateFormat.format(time);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    /**
     * 时间延迟并且时间字符串里要加个T
     * @return
     */
    public static String timeLateTProcess(){
        String time = timeAddMinute("YYYY-MM-dd ",20);
        return time.replace(" ","T");
    }
}
