package com.hj.outbound.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hj.common.dto.TestDataBean;
import com.hj.common.utils.*;
import com.hj.outbound.common.PubCommonAction;
import com.hj.outbound.common.ReadTestData;
import com.hj.outbound.dto.VariableDataBean;
import com.hj.outbound.mapper.test_db.CaseResultCheckMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SaaS慧声语音系统的接口请求
 */
@Service
public class SaaSShApiService {

    Logger logger = LoggerFactory.getLogger(SaaSShApiService.class);

    @Resource
    CaseResultCheckMapper caseResultCheckMapper;

    @Autowired
    SaaSShApiServicePublicOperate saaSShApiServicePublicOperate;

    protected ApiRequestUtils requestUtils = new ApiRequestUtils();

    protected PubCommonAction pubCommonAction = new PubCommonAction(requestUtils);

    protected VariableDataBean variableDataBean;

    protected Map<String,Object> variableMap;

    protected static PropertyUtil propertyUtil = new PropertyUtil(PlatformConstants.SaaS_SH_VOICE_API_URL);

    public void initVariableData( VariableDataBean variableDataBean,Map<String,Object> variableMap) {
        this.variableMap = variableMap;
        this.variableDataBean = variableDataBean;
    }

    /**退出登录**/
    public void loginOutApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("loginOut.uri"));
    }

    /**
     * 登录接口
     */
    public void loginApi() {
        //获取testDataBean对象
        TestDataBean testDataBean = ReadTestData.getInstance().getSingleApiData(1, Constant.SaaSShTable);
        //请求
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("login.uri"));
        //断言
        CheckPointUtils.checkPoints(responseBody, JSONObject.parseObject(testDataBean.getChecks()), testDataBean.getApiDescription());
    }

    /**
     * 机构管理的角色用户登录接口
     */
    public void organizationLoginApi(TestDataBean testDataBean) {
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("login.uri"));
        //断言
        CheckPointUtils.checkPoints(responseBody, JSONObject.parseObject(testDataBean.getChecks()), testDataBean.getApiDescription());
    }

    /**
     * 获取行业分类列表接口
     */
    public void getIndustryCategoryApi(TestDataBean testDataBean) {

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("getIndustryCategory.uri"));
        //后续添从列表查出新增的行业
    }

    /**
     * 添加行业知识库接口
     */
    public void addIndustryKnowledgeApi(TestDataBean testDataBean) {
        requestUtils.getSaveLinked().put("industryAssortId","1");//临时作用,verbal_industry表 行业id
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addIndustryKnowledge.uri"));

        int goalCountDB = caseResultCheckMapper.getIndustryKnowledgeCount(variableDataBean.getIndustryKnowledgeName());
        pubCommonAction.checkPointFromDBQueryGoal(goalCountDB,testDataBean.getApiDescription());
    }

    /**
     * 添加行业话术接口
     */
    public void addIndustryDiscourseApi(TestDataBean testDataBean) {
        String assistField = testDataBean.getAssist();
        String randomIndustryDiscourseName = "自动"+RandomUtil.randomStr(3);
        if(null != assistField &&! "".equals(assistField)){
            //随机生成行业话术名
            String newRequestParam = testDataBean.getRequestData().replace("行业话术", randomIndustryDiscourseName);
            testDataBean.setRequestData(newRequestParam);
        }
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addIndustryDiscourse.uri"));
        String industryDiscourseName = null == assistField || "".equals(assistField)?variableDataBean.getIndustryDiscourseName():assistField;
        int goalCountDB = caseResultCheckMapper.getIndustryKnowledgeItemCount(industryDiscourseName);
        pubCommonAction.checkPointFromDBQueryGoal(goalCountDB, testDataBean.getApiDescription());
    }

    /**
     * 行业知识库列表接口
     */
    public void industryKnowledgeListApi(TestDataBean testDataBean) {

        requestUtils.getSaveLinked().put("industryAssortId","1");//临时作用,verbal_industry表 行业id
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("industryKnowledgeList.uri"));
        int industryKnowledgeCount = Integer.parseInt(JsonPathRead.readJson(responseBody, testDataBean.getChecks(), testDataBean.getApiDescription()));

        int allCountDB = caseResultCheckMapper.getAllIndustryKnowledgeCount();
        pubCommonAction.checkPointFromDBQueryAll(allCountDB,industryKnowledgeCount,testDataBean.getApiDescription());
    }

    /**
     * 分页查找子行业下的话术列表接口
     */
    public void subIndustryDiscourseListApi(TestDataBean testDataBean) {

        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("subIndustryDiscourseList.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**
     * 修改行业话术接口
     */
    public void updateIndustryDiscourseApi(TestDataBean testDataBean) {

        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("updateIndustryDiscourse.uri"));
        String modifyIndustryDiscourseName = JsonPathRead.readJson(testDataBean.getRequestData(), "$.name", testDataBean.getApiDescription());

        int industryKnowledgeItemTableCount = caseResultCheckMapper.getIndustryKnowledgeItemCount(modifyIndustryDiscourseName);
        pubCommonAction.checkPointFromDBQueryGoal(industryKnowledgeItemTableCount, testDataBean.getApiDescription());
    }

    /**
     * 新增开放性话术
     */
    public void addOpenDiscourseApi(TestDataBean testDataBean) {
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addOpenDiscourse.uri"));
        int openVerbalTableCount = caseResultCheckMapper.getOpenVerbalCount(variableDataBean.getOpenDiscourseName());
        pubCommonAction.checkPointFromDBQueryGoal(openVerbalTableCount,testDataBean.getApiDescription());
    }

    /**
     * 开放性话术列表
     */
    public void openDiscourseListApi(TestDataBean testDataBean) {
        String apiDescription = testDataBean.getApiDescription();
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("openDiscourseList.uri"));

        String checkPoints[] = testDataBean.getChecks().split(",");
        String openDiscourseNameJP = checkPoints[0];
        String openDiscourseName = JsonPathRead.readJson(responseBody, openDiscourseNameJP,apiDescription);
        CheckPointUtils.myAssertEquals(apiDescription, openDiscourseName, variableDataBean.getOpenDiscourseName());
        PubCommonAction.paginationCheck(responseBody,apiDescription, checkPoints, 0,1);
    }

    /**
     * 开放性话术-编辑开放性话术
     */
    public void editOpenDiscourseApi(TestDataBean testDataBean) {
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("editOpenDiscourse.uri"));
        String editOpenDiscourseName = JsonPathRead.readJson(testDataBean.getRequestData(), "$.name");
        int openVerbalTableCount = caseResultCheckMapper.getOpenVerbalCount(editOpenDiscourseName);
        pubCommonAction.checkPointFromDBQueryGoal(openVerbalTableCount,testDataBean.getApiDescription());
    }

    /**
     * 开放性话术-删除开放性话术
     */
    public void deleteOpenDiscourseApi(TestDataBean testDataBean) {
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteOpenDiscourse.uri"));
        int countDB = caseResultCheckMapper.getOpenVerbalCount(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(countDB,testDataBean.getApiDescription());
    }

    /**
     * 开放性话术-批量确认开放性话术
     */
    public void batchCheckOpenDiscourseApi(TestDataBean testDataBean) {

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("batchCheckOpenDiscourse.uri"));
    }

    /**
     * 开放性话术-导出开放性话术
     */
    public void exportOpenDiscourseApi(TestDataBean testDataBean) {
        if(testDataBean.getApiDescription().contains("单个")){
            pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        }
        testDataBean.setUrl(propertyUtil.getproperValue("exportOpenDiscourse.uri"));
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        Header[] headers = closeableHttpResponse.getHeaders("Content-Type");
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(), headers[0].getValue(), testDataBean.getChecks());
    }

    /**开放性话术-计划批次列表**/
    public void planBatchListApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("planBatchList.uri"));
    }

    /**
     * 添加坐席组
     */
    public void addSeatGroupApi(TestDataBean testDataBean) {
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addSeatGroup.uri"));

        int agentGroupTableCount = caseResultCheckMapper.getAgentGroupCount(variableDataBean.getSeatGroupName());
        pubCommonAction.checkPointFromDBQueryGoal(agentGroupTableCount, testDataBean.getApiDescription());
    }

    /**
     * 坐席组列表
     */
    public void seatGroupListApi(TestDataBean testDataBean) {
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("seatGroupList.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**
     * 删除坐席组
     **/
    public void deleteSeatGroupApi(TestDataBean testDataBean) {

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteSeatGroup.uri"));
    }

    /**
     * 添加坐席
     **/
    public void addSeatApi(TestDataBean testDataBean) {
        // 后续完成请求参数变量赋值
        //  后续完成 checkPointFromDB();
    }

    /**
     * 坐席列表
     **/
    public void seatListApi(TestDataBean testDataBean) {

        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("seatList.uri"));
    }

    /**
     * 坐席状态切换
     **/
    public void switchSeatStatusApi(TestDataBean testDataBean) {
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("switchSeatStatus.uri"));
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }

    /**
     * 客户管理-添加客户名单
     */
    public void addDialCustomerApi(TestDataBean testDataBean){
        testDataBean.setRequestData(testDataBean.getRequestData().replace("客户字典姓名", variableDataBean.getCustomerName()));
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addDialCustomer.uri"));
        int crmCustomerTableCount = caseResultCheckMapper.getCrmCustomerCount(variableDataBean.getCustomerName());
        pubCommonAction.checkPointFromDBQueryGoal(crmCustomerTableCount,testDataBean.getApiDescription());
    }

    /**客户管理-添加一键外呼任务**/
    public void selectCustomerNewCallPlanApi(TestDataBean testDataBean, List<String> callTemplateIds,List<String> customerIds){
        Map<String, Object> checkMap = pubCommonAction.requestDataInitialize(testDataBean, variableMap);
        JSONObject requestJsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        requestJsonObject.put("callTemplateIds",callTemplateIds);
        requestJsonObject.put("ids",customerIds);
        testDataBean.setRequestData(requestJsonObject.toString());
        String body = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("selectCustomerNewCallPlan.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }
    /**
     * 客户管理-查询拨打客户名单
     */
    public void queryDialCustomerApi(TestDataBean testDataBean){
        if(testDataBean.getApiDescription().contains("查询所有")){
            String allCrmCustomerCount = String.valueOf(caseResultCheckMapper.getAllCrmCustomerCount());
            variableMap.put("allCustomerCount",allCrmCustomerCount);
        }
        else if(testDataBean.getApiDescription().contains("批次号查询")){
            //通过姓名搜索查询到crm_customer_batch_id 存入批次id
            String customerBatchId = caseResultCheckMapper.getCrmCustomerBatchId(variableDataBean.getCustomerName());
            String batchNumber = caseResultCheckMapper.getCrmCustomerBatchNumber(customerBatchId);
            requestUtils.getSaveLinked().put("batchNumber",batchNumber);
        }
        Map<String,Object> checkMap = pubCommonAction.requestDataInitialize(testDataBean,variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("queryDialCustomer.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**客户管理-下载模板-客户名单**/
    public void downloadCustomerTemplateApi(TestDataBean testDataBean){
        String statusCode = testDataBean.getChecks().split(",")[0];
        String fileType =  testDataBean.getChecks().split(",")[1];
        testDataBean.setUrl(propertyUtil.getproperValue("downloadCustomerTemplate.uri"));
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        String headCode = String.valueOf(closeableHttpResponse.getStatusLine().getStatusCode());
        HttpEntity entity = closeableHttpResponse.getEntity();
        long contentLength = entity.getContentLength();

        Header[] headers = closeableHttpResponse.getHeaders("Content-Type");
        String bodyFileType = headers[0].getValue();

        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(), headCode,statusCode);
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(), bodyFileType,fileType);
        if(contentLength == 0){
            MyLogger.myResponseLogger("下载模板接口的数据量为0");
            CheckPointUtils.myAssertFalse("下载模板接口的数据量为0");
        }
    }

    /**客户管理-导入拨打的客户名单**/
    public void importCustomersApi(TestDataBean testDataBean){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("file",PlatformConstants.CUSTOMER_TEMPLATE);
        testDataBean.setRequestData(jsonObject.toString());
        testDataBean.setUrl(propertyUtil.getproperValue("importTemp.uri"));
        String body = requestUtils.fileUpLoad(testDataBean);
        CheckPointUtils.checkResponseCodeMsg(body,testDataBean.getApiDescription());
    }

    /**
     * 客户管理-确认导入客户名单
     */
    public void confirmImportCustomerApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("confirmImportCustomer.uri"));
    }

    /**
     * 客户管理-查询机构所有意向标签
     */
    public void queryOrganizationIntentionLabApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("queryOrganizationIntentionLab.uri"));
    }

    /**一键外呼-查看发布(可用)的模板**/
    public void publishedCallTemplateApi(TestDataBean testDataBean){
        String body = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("publishedCallTemplate.uri"));
        List<String> publishedTemplateIdList = new ArrayList<>();
        //遍历data数组的所有可用的模板id
        JSONObject jsonObject= JSONObject.parseObject(body);
        JSONArray dataJsonArray =jsonObject.getJSONArray("data");
        //遍历出目标的已发布的话术模板id
        for(int i = 0;i<dataJsonArray.size();i++){
            JSONObject publishedTemplateIdJson = dataJsonArray.getJSONObject(i);
            String callTemplateId = publishedTemplateIdJson.getString("callTemplateId");
            String publishedTemplateId = publishedTemplateIdJson.getString("publishedTemplateId");
            if(callTemplateId.equals(requestUtils.getSaveLinked().get("discourseTemplateId"))){
                requestUtils.getSaveLinked().put("publishedTemplateId",publishedTemplateId);
            }
            publishedTemplateIdList.add(publishedTemplateId);
        }
        requestUtils.getSaveLinked().put("publishedTemplateIds",publishedTemplateIdList);
    }

    /**一键外呼-获取模板条件数据**/
    public void getDictByPublishIdsApi(TestDataBean testDataBean){

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("publishedCallTemplateIds",requestUtils.getSaveLinked().get("publishedTemplateIds"));
        testDataBean.setRequestData(jsonObject.toJSONString());
        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("getDictByPublishIds.uri"));
    }

    /**
     * 创建话术模板
     */
    public void addDiscourseTemplateApi(TestDataBean testDataBean){

        requestUtils.getSaveLinked().put("industryAssortId","1");//临时作用,verbal_industry表 行业id
        requestUtils.getSaveLinked().put("classId","1");//临时作用 verbal_classify表

        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addDiscourseTemplate.uri"));

        int discourseTemplateTableCount = caseResultCheckMapper.getDiscourseTemplateCount(variableDataBean.getDiscourseTemplateName(),0);
        pubCommonAction.checkPointFromDBQueryGoal(discourseTemplateTableCount,testDataBean.getApiDescription());
    }

    /**
     * 复制话术模板
     */
    public void copyDiscourseTemplateApi(TestDataBean testDataBean){
        pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue( "copyDiscourseTemplate.uri"));
        int discourseTemplateTableCount = caseResultCheckMapper.getDiscourseTemplateCount(variableDataBean.getDiscourseTemplateName(),0);
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),discourseTemplateTableCount,2);
    }

    /**话术模板详情**/
    public void discourseTemplateDetailsApi(TestDataBean testDataBean){

        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue( "discourseTemplateDetails.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**可用的话术模板列表-已训练的**/
    public void usableDiscourseTemplateListApi(TestDataBean testDataBean){

        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("usableDiscourseTemplateList.uri"));
        int callStrategyCount = Integer.parseInt(JsonPathRead.readJson(responseBody,testDataBean.getChecks()));
        //校验返回的数量和数据库记录数量是否一致
        int callStrategyTableCountAll = caseResultCheckMapper.getPublishDiscourseTemplateCount();
        pubCommonAction.checkPointFromDBQueryAll(callStrategyTableCountAll,callStrategyCount,testDataBean.getApiDescription());
    }

    /**
     * 话术模板列表-搜索
     */
    public void discourseTemplateListApi(TestDataBean testDataBean){

        pubCommonAction.requestParamInitialize(testDataBean,variableMap);

        if(testDataBean.getApiDescription().contains("搜索全部")){
            String recordsLengthJsonPath = testDataBean.getChecks().split(",")[0];
            String totalJsonPath= testDataBean.getChecks().split(",")[1];
            String sizeJsonPath = testDataBean.getChecks().split(",")[2];
            String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("discourseTemplateList.uri"));
            int bodyRecordsLength = Integer.parseInt(JsonPathRead.readJson(responseBody, recordsLengthJsonPath, testDataBean.getApiDescription()));
            int bodyTotal = Integer.parseInt(JsonPathRead.readJson(responseBody, totalJsonPath, testDataBean.getApiDescription()));
            int bodySize = Integer.parseInt(JsonPathRead.readJson(responseBody, sizeJsonPath, testDataBean.getApiDescription()));
            CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),bodyRecordsLength,bodySize);
            int discourseTemplateTableCountAll = caseResultCheckMapper.getAllDiscourseTemplateCount();
            pubCommonAction.checkPointFromDBQueryAll(discourseTemplateTableCountAll,bodyTotal,testDataBean.getApiDescription());
        }
        else {
            Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
            checkMap = ParamRequestData.paramRequestData(variableMap,checkMap);
            testDataBean.setChecks(checkMap.toString());
            String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("discourseTemplateList.uri"));
            CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
            if(testDataBean.getApiDescription().contains("搜索复制的话术模板")){
                JsonPathRead.readJson(responseBody,"$.data.records[0].mainFlowId",testDataBean.getApiDescription());
            }
        }
    }

    /**
     * 删除话术模板
     */
    public void deleteDiscourseTemplateApi(TestDataBean testDataBean){
        pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteDiscourseTemplate.uri"));
        int count = caseResultCheckMapper.getDiscourseTemplateCount(variableDataBean.getDiscourseTemplateName(), 1);
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),count,1);
    }

    /**话术模板-话术全局设置信息**/
    public void  discourseGoalSetInfoApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("discourseGoalSetInfo.uri"));
    }

    /**话术模板-发布话术模板**/
    public void  publishDiscourseTemplateApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean,variableMap);
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("publishDiscourseTemplate.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }


    /**
     * 添加意向等级接口
     */
    public void addIntentionRankApi(TestDataBean testDataBean) {
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addIntentionRank.uri"));
        int templateIntentionRankTableCount = caseResultCheckMapper.getTemplateIntentionRankCount(variableDataBean.getIntentionRankName());
        pubCommonAction.checkPointFromDBQueryGoal(templateIntentionRankTableCount,testDataBean.getApiDescription());
    }

    /**
     * 修改意向等级的优先级接口
     */
    public void modifyIntentionRankSortApi(TestDataBean testDataBean) {

    }

    /**
     * 删除意向等级接口
     */
    public void deleteIntentionRankApi(TestDataBean testDataBean) {

        pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteIntentionRank.uri"));
    }

    /**
     * 获取意向等级详情接口
     */
    public void getIntentionRankDetailsApi(TestDataBean testDataBean) {
        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        String responseBody = pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("getIntentionRankDetails.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**
     * 查询意向等级列表
     */
    public void getIntentionRankListApi(TestDataBean testDataBean) {

        String apiDescription = testDataBean.getApiDescription();
        String responseBody = pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("queryIntentionRankList.uri"));

        JSONObject responseBodyJsonObject =JSONObject.parseObject(responseBody);
        JSONArray dataArray = responseBodyJsonObject.getJSONArray("data");
        if(dataArray.size() == 0){
            CheckPointUtils.myAssertFalse("【"+apiDescription+"】接口：意向等级列表下无记录数");
        }
        for(int i =0;i<dataArray.size();i++){
            String intentionRankName = dataArray.getJSONObject(i).getString("intentionRankName");
            if(intentionRankName.equalsIgnoreCase(variableDataBean.getIntentionRankName())){
                break;
            }
            else if(i == dataArray.size()-1){
                CheckPointUtils.myAssertFalse("【"+apiDescription+"】接口：意向等级列表下目标记录");
            }
        }
    }

    /**
     * 获取意向等级的下拉框列表接口
     */
    public void getIntentionRankDrownListApi(TestDataBean testDataBean) {

        pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("getIntentionRankDrownList.uri"));
    }

    /**
     * 更新意向等级接口
     */
    public void updateIntentionRankApi(TestDataBean testDataBean) {

        String apiDescription = testDataBean.getApiDescription();
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("updateIntentionRank.uri"));
        String intentionRankName = JsonPathRead.readJson(testDataBean.getRequestData(), "$.intentionRankName", apiDescription);
        int templateIntentionRankTableCount = caseResultCheckMapper.getTemplateIntentionRankCount(intentionRankName);
        pubCommonAction.checkPointFromDBQueryGoal(templateIntentionRankTableCount,apiDescription);
    }

    /**
     * 话术词典-添加客户字典
     */
    public void addCustomerDictApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addCustomerDict.uri"));
        String discourseTemplateId = requestUtils.getSaveLinked().get("discourseTemplateId").toString();
        int templateCustomerDictTableCount;
        if(JsonPathRead.readJson(testDataBean.getRequestData(),"$.name").contains("$")){
            templateCustomerDictTableCount  = caseResultCheckMapper.getCustomerDictCount(variableDataBean.getCustomerDictName(),discourseTemplateId);
        }
        else{
            templateCustomerDictTableCount  = caseResultCheckMapper.getCustomerDictCount(JsonPathRead.readJson(testDataBean.getRequestData(),"$.name"),discourseTemplateId);
        }
        pubCommonAction.checkPointFromDBQueryGoal(templateCustomerDictTableCount,testDataBean.getApiDescription());
    }

    /**
     * 话术词典-客户字典列表接口
     */
    public void customerDictListApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.getPubActionProcess(testDataBean, propertyUtil.getproperValue("customerDictList.uri"));
        if(responseBody.contains(variableDataBean.getCustomerDictName())){
            MyLogger.myResponseLogger("【"+testDataBean.getApiDescription()+"】接口响应中包含了期望数据记录");
        }
        else{
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口响应中不包含期望数据记录名："+variableDataBean.getCustomerDictName());
        }
    }

    /**
     * 话术词典-客户字典(无分页)
     */
    public void customerDictNotPageApi(TestDataBean testDataBean){

        pubCommonAction.getPubActionProcess(testDataBean, propertyUtil.getproperValue("customerDictNotPage.uri"));
    }

    /**
     * 话术词典-客户字典启动、禁用
     * @param testDataBean
     */
    public void customerDictEnableApi(TestDataBean testDataBean){
        String discourseTemplateId = requestUtils.getSaveLinked().get("discourseTemplateId").toString();
        pubCommonAction.getPubActionProcess(testDataBean, propertyUtil.getproperValue("customerDictEnable.uri"));
        int status = Integer.parseInt(caseResultCheckMapper.getCustomerDictStatus(variableDataBean.getCustomerDictName(),discourseTemplateId));
        if(testDataBean.getApiDescription().contains("启用")){
            CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),status,1);
        }
        else{
            CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),status,0);
        }
    }

    /**
     * 拨打计划-查看外呼计划任务
     */
    public void callPlanTaskApi(TestDataBean testDataBean){
        Map <String,Object> checkMap = pubCommonAction.requestDataInitialize(testDataBean,variableMap);
        String responseBody =  pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("callPlanTask.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
        //检查该批次下面的电话簿数量
        //已处理的号码数量
        int alreadyProcessedCount = Integer.parseInt(JsonPathRead.readJson(responseBody,"$.data.records[0].alreadyProcessedCount"));
        //等待处理的号码数量
        int waitProcessCount =Integer.parseInt(JsonPathRead.readJson(responseBody,"$.data.records[0].waitProcessCount"));
        //处理中的号码数量
        int inProcessCount = Integer.parseInt(JsonPathRead.readJson(responseBody,"$.data.records[0].inProcessCount"));
        // 总号码数量
        int allCustomerList = alreadyProcessedCount+waitProcessCount+inProcessCount;
        //期望值是2，是因为在SaaSShCombinationApi类中客户名单循环添加2次
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),allCustomerList,2);
    }

    /**
     * 拨打计划-外呼计划任务批次列表
     */
    public void callPlanTaskBatchListApi(TestDataBean testDataBean){
        JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        jsonObject.put("callPlanId",caseResultCheckMapper.getCallPlanId(variableDataBean.getDialPlanName()));
        testDataBean.setRequestData(jsonObject.toString());
        Map <String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String responseBody =  pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("callPlanBatchList.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**
     * 拨打计划-批次号码查看(客户名单信息)
     */
    public void viewPlanTaskBatchNumberApi(TestDataBean testDataBean){
        Map <String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String responseBody =  pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("BatchDetail.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**拨打计划-导出批次客户**/
    public void exportBatchCustomerListApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("exportCustomerList.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**拨打计划-外呼任务-批次详情-导出(消息列表)**/
    public void messageListApi(TestDataBean testDataBean){
        for(int i = 0; i < 10; i++){
            logger.info("导出批次客户下载中，需要等待10秒，稍等中........");
            SleepUtil.sleep();
        }
        //导出批次客户后不是实时的能在消息列表里看到
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("messageList.uri"));
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
        String downloadLink = JsonPathRead.readJson(body,"$.data.records[0].downloadLink");
        requestUtils.getSaveLinked().put("downloadLink",downloadLink);
    }

    /**拨打计划-批次空号检测**/
    public void emptyNumberCheckApi(TestDataBean testDataBean){
        SleepUtil.sleep();
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("emptyNumberCheck.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }

    /**拨打计划-查看批次检测状态**/
    public void batchCheckStatusApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("batchCheckStatus.uri"));
        String status = JsonPathRead.readJson(body, "$.data");
        //需要校验下检测状态
        String statusDB = caseResultCheckMapper.getBatchNullNumerCheckStatus(requestUtils.getSaveLinked().get("batchId").toString());
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),status,statusDB);
    }

    /**拨打计划-消息列表-下载文件**/
    public void downloadFileApi(TestDataBean testDataBean){
        testDataBean.setUrl(propertyUtil.getproperValue("downloadFile.uri"));
        pubCommonAction.fileStreamProcess(testDataBean);
    }

    /**拨打计划-消息列表-删除消息**/
    public void deleteMsgApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteMsg.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }

    /**拨打计划-暂停呼叫任务**/
    public void pauseCallTaskApi(TestDataBean testDataBean){

        pubCommonAction.stopCallTask(testDataBean,caseResultCheckMapper,variableDataBean.getDialPlanName(),propertyUtil.getproperValue("pauseTask.uri"));
    }

    /**拨打计划-继续呼叫任务**/
    public void continueCallTaskApi(TestDataBean testDataBean){
        pubCommonAction.getCallPlanStatusAlreadyStop(caseResultCheckMapper, variableDataBean.getDialPlanName(),120);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("continueTask.uri"));
        pubCommonAction.continueCallCheckStatus(caseResultCheckMapper, variableDataBean.getDialPlanName());
    }

    /**拨打计划-批次操作**/
    public void batchOperationApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("batchOperation.uri"));
    }

    /**拨打计划-二次呼叫,业务结果获取**/
    public void getIntentionRankApi(TestDataBean testDataBean){
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("getIntentionRank.uri"));
        int list = Integer.parseInt(JsonPathRead.readJson(body, "$.data.length()"));
        if(list == 0){
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口响应body的data数据为空");
        }
    }

    /**通话记录-查询**/
    public void dialogueRecordListApi(TestDataBean testDataBean){
        Map<String,String> fieldsMap = new HashMap<>();
        Map<String,String> errorFieldsMap = new HashMap<>();
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        String responseBody =  pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("dialogueRecordList.uri"));

        int count = caseResultCheckMapper.getDialogueRecordCount(Integer.parseInt(variableDataBean.getPhoneNum()));
        int countDB = Integer.parseInt(JsonPathRead.readJson(responseBody, "$.data.records.length()"));
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),count,countDB);

        //针对重要字段进行断言
        JSONObject jsonObjectBody = JSONObject.parseObject(responseBody);
        JSONObject dataJsonObject = (JSONObject) jsonObjectBody.get("data");
        JSONArray recordsJsonArray = dataJsonObject.getJSONArray("records");
        for (int i = 0 ;i<recordsJsonArray.size();i++){
            JSONObject jsonObject = recordsJsonArray.getJSONObject(i);

            fieldsMap.put("phoneNumber",jsonObject.getString("phoneNumber"));
            fieldsMap.put("callStatus",jsonObject.getString("callStatus"));
            fieldsMap.put("callTemplateId",jsonObject.getString("callTemplateId"));
            fieldsMap.put("callTemplateName",jsonObject.getString("callTemplateName"));
            fieldsMap.put("callPlanName",jsonObject.getString("callPlanName"));
            fieldsMap.put("flowNumber",jsonObject.getString("flowNumber"));
            fieldsMap.put("callTime",jsonObject.getString("callTime"));
            fieldsMap.put("hangupTime",jsonObject.getString("hangupTime"));
            fieldsMap.put("callDuration",jsonObject.getString("callDuration"));
            fieldsMap.put("interactiveCount",jsonObject.getString("interactiveCount"));

            JSONObject customerInfoMap = jsonObject.getJSONObject("customerInfoMap");
            if(customerInfoMap.size()>0 && StringUtils.isNotBlank(customerInfoMap.toString())){
                MyLogger.myResponseLogger("【"+testDataBean.getApiDescription()+"】接口的customerInfoMap字段不为空校验成功");
            }
            else{
                CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口的customerInfoMap字段不为空校验失败");
            }
            for(Map.Entry<String,String> map :fieldsMap.entrySet()){
                    if(!StringUtils.isNotBlank(map.getValue())){
                        errorFieldsMap.put(map.getKey(),map.getValue());
                    }
            }
            if(errorFieldsMap.size()>0){
                CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口的字段不为空校验失败,校验失败的字段有："+errorFieldsMap.toString());
            }
            else{
                MyLogger.myResponseLogger("【"+testDataBean.getApiDescription()+"】接口的字段不为空校验成功");
            }
            fieldsMap.clear();
        }
    }

    /**通话记录-详情**/
    public void dialogueDetailApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("dialogueDetail.uri"));
        //后续调试再确认是否需要添加断言
    }

    /**通话记录-获取通话录音**/
    public void dialogueDetailGetRecordApi(TestDataBean testDataBean){

        //pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("getDialRecord.uri"));
        testDataBean.setUrl(propertyUtil.getproperValue("getDialRecord.uri"));
        pubCommonAction.fileStreamProcess(testDataBean);
    }

    /**通话记录-查询条件数据**/
    public void queryConditionDataApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("queryConditionData.uri"));
    }

    /**通话记录-话单导出**/
    public void exportCRDExcelApi(TestDataBean testDataBean){
        JSONObject jsonObjectData = JSONObject.parseObject(testDataBean.getRequestData());
        String publishedTemplateId = requestUtils.getSaveLinked().get("publishedTemplateId").toString();
        List<String> list =  new ArrayList<>();
        list.add(publishedTemplateId);
        jsonObjectData.put("templateList",list);
        testDataBean.setRequestData(jsonObjectData.toString());
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("exportCRDExcel.uri"));
        CheckPointUtils.checkPoints(responseBody,JSONObject.parseObject(testDataBean.getChecks()),testDataBean.getApiDescription());
    }

    /**通话记录-导出记录**/
    public void exportExcelList(TestDataBean testDataBean){

       pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("exportExcelList.uri"));
    }

    /**通话记录-导出记录-获取表头**/
    public void exportExcelHeadApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("exportExcelHead.uri"));
    }

    /**新增自定义知识库**/
    public void addKnowledgeApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addKnowledge.uri"));
        String assistField = testDataBean.getAssist();
        String knowledgeName = null == assistField || assistField.equals("")?variableDataBean.getKnowledgeName():assistField;
        int templateKnowledgeTableCount = caseResultCheckMapper.getKnowledgeCount(knowledgeName,0);
        if(testDataBean.getApiDescription().contains("异常场景")){
            pubCommonAction.checkPointFromDBQueryForExceptionCase(templateKnowledgeTableCount,testDataBean.getApiDescription());
        }
        else{
            pubCommonAction.checkPointFromDBQueryGoal(templateKnowledgeTableCount,testDataBean.getApiDescription());
        }

    }
    /**自定义知识库列表**/
    public void knowledgeListApi(TestDataBean testDataBean){
        Map<String, Object> checkMap = pubCommonAction.requestDataInitialize(testDataBean, variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("knowledgeList.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }
    /**自定义知识库详情**/
    public void knowledgeDetailApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.getPubActionProcess(testDataBean, propertyUtil.getproperValue("knowledgeDetail.uri"));
        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }
    /**更新自定义知识库**/
    public void updateKnowledgeApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("updateKnowledge.uri"));
        int templateKnowledgeTableCount = caseResultCheckMapper.getKnowledgeCount(testDataBean.getAssist(),0);
        pubCommonAction.checkPointFromDBQueryGoal(templateKnowledgeTableCount,testDataBean.getApiDescription());
    }
    /**删除自定义知识库**/
    public void deleteKnowledgeApi(TestDataBean testDataBean){
        pubCommonAction.deletePubActionProcess(testDataBean, propertyUtil.getproperValue("deleteKnowledge.uri"));
        int templateKnowledgeCount = caseResultCheckMapper.getKnowledgeCount(variableDataBean.getKnowledgeName(),1);
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(templateKnowledgeCount,testDataBean.getApiDescription());
    }
    /**导入知识库**/
    public void importKnowApi(TestDataBean testDataBean){
        JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        if(testDataBean.getApiDescription().contains("(行业知识库")){
            jsonObject.put("industryKnowLedgeIds",requestUtils.getSaveLinked().get("industryKnowLedgeIds"));
        }
        else {//开放性话术
            jsonObject.put("openVerbalIds",requestUtils.getSaveLinked().get("openDiscourseIds"));
        }
        testDataBean.setRequestData(jsonObject.toJSONString());
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("importKnow.uri"));
    }

    /**话术分类列表**/
    public void discourseClassListApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("discourseClassList.uri"));
        int bodyCount = Integer.parseInt(JsonPathRead.readJson(responseBody,testDataBean.getChecks(),testDataBean.getApiDescription()));
        int verbalClassifyTableCountAll = caseResultCheckMapper.getAllVerbalClassifyCount();
        pubCommonAction.checkPointFromDBQueryAll(verbalClassifyTableCountAll,bodyCount,testDataBean.getApiDescription());
    }

    /**行业分类列表**/
    public void industryClassListApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("industryClassList.uri"));
        int bodyCount = Integer.parseInt(JsonPathRead.readJson(responseBody,testDataBean.getChecks(),testDataBean.getApiDescription()));
        int verbalIndustryTableCountAll = caseResultCheckMapper.getAllVerbalIndustryCount();
        pubCommonAction.checkPointFromDBQueryAll(verbalIndustryTableCountAll,bodyCount,testDataBean.getApiDescription());
    }

    /**策略管理-添加拨打策略**/
    public void addCallStrategyApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("addCallStrategy.uri"));
        String assistField = testDataBean.getAssist();
        String dialStrategyName = null == assistField || assistField.equals("")?variableDataBean.getDialStrategyName():assistField;
        int callStrategyTableCount = caseResultCheckMapper.getCallStrategyCount(dialStrategyName);
        pubCommonAction.checkPointFromDBQueryGoal(callStrategyTableCount,testDataBean.getApiDescription());
    }

    /**策略管理-拨打策略列表**/
    public void callStrategyListApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("callStrategyList.uri"));
        int bodyCount =  Integer.parseInt(JsonPathRead.readJson(responseBody,testDataBean.getChecks()));
        int callStrategyTableCountAll = caseResultCheckMapper.getAllCallStrategyCount();
        pubCommonAction.checkPointFromDBQueryAll(callStrategyTableCountAll,bodyCount,testDataBean.getApiDescription());
        //断言每个策略列表里的策略名称是否存在
        JSONObject jsonObject = JSONObject.parseObject(responseBody);
        JSONArray dataArray = jsonObject.getJSONArray("data");

        for(int i = 0;i<dataArray.size();i++){
            JSONObject object = (JSONObject) dataArray.get(i);
            String strategyName = object.getString("strategyName");
            String id = object.getString("id");
            if(!StringUtils.isNotBlank(strategyName) || !StringUtils.isNotBlank(id) ){
                CheckPointUtils.myAssertFalse("拨打策略列表响应内容的策略名称值或id显示null");
            }
        }
    }

    /**策略管理-可用的线路列表**/
    public void callStrategyPhoneLineListApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("callStrategyPhoneLineList.uri"));
        int bodyCount = Integer.parseInt(JsonPathRead.readJson(responseBody,testDataBean.getChecks(),testDataBean.getApiDescription()));
        int phoneLineTableCountAll = caseResultCheckMapper.getAllPhoneLineCountAll();
        pubCommonAction.checkPointFromDBQueryAll(phoneLineTableCountAll,bodyCount,testDataBean.getApiDescription());
    }

    /**策略管理-删除拨打策略**/
    public void deleteCallStrategyApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteCallStrategy.uri"));
        TestDataBean callStrategyListDataBean = ReadTestData.getInstance().getSingleApiData(58,Constant.SaaSShTable);
        String responseBody = pubCommonAction.postPubActionProcess(callStrategyListDataBean, propertyUtil.getproperValue("callStrategyList.uri"));
        //断言每个策略列表里的策略名称是否存在
        JSONObject jsonObject = JSONObject.parseObject(responseBody);
        JSONArray dataArray = jsonObject.getJSONArray("data");

        for(int i = 0;i<dataArray.size();i++){
            JSONObject object = (JSONObject) dataArray.get(i);
            String strategyName = object.getString("strategyName");
            if(strategyName.equals(variableDataBean.getDialStrategyName())){
                CheckPointUtils.myAssertFalse("【"+variableDataBean.getDialStrategyName()+"】(拨打策略已删除，却在响应body里还显示");
            }
        }
        MyLogger.myResponseLogger("再次请求列表接口返回的数据里没有已删除的记录了");
    }

    /**策略管理-添加多轮策略**/
    public void addMultipleCallStrategyApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addMultipleCallStrategy.uri"));
        int multipleCallStrategyTableCount = caseResultCheckMapper.getMultipleCallStrategyCount(variableDataBean.getMultipleDialStrategyName());
        pubCommonAction.checkPointFromDBQueryGoal(multipleCallStrategyTableCount,testDataBean.getApiDescription());
    }

    /**策略管理-删除多轮策略**/
    public void deleteMultipleCallStrategyApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteMultipleCallStrategy.uri"));
        int multipleCallStrategyTableCount = caseResultCheckMapper.getMultipleCallStrategyCount(variableDataBean.getMultipleDialStrategyName());
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),multipleCallStrategyTableCount,0);
    }

    /**策略管理-可用的多轮策略**/
    public void usableMultipleCallStrategyApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("usableMultipleCallStrategy.uri"));
    }

    /**策略管理-分页查询多轮策略**/
    public void pageMultipleCallStrategyApi(TestDataBean testDataBean){
        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("pageMultipleCallStrategy.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**策略管理-多轮策略-添加轮次**/
    public void multipleCallStrategyAddItemApi(TestDataBean testDataBean){
        String newRequestData = testDataBean.getRequestData().replace("拨打时间", DateTimeUtil.timeAddMinute("",5));
        testDataBean.setRequestData(newRequestData);
        saaSShApiServicePublicOperate.multipleCallStrategyItem(testDataBean,pubCommonAction,caseResultCheckMapper,variableDataBean,propertyUtil.getproperValue("multipleCallStrategyAddItem.uri"));
    }

    /**策略管理-多轮策略-编辑轮次**/
    public void multipleCallStrategyEditItemApi(TestDataBean testDataBean){
        String newRequestData = testDataBean.getRequestData().replace("拨打时间", DateTimeUtil.timeAddMinute("",5));
        testDataBean.setRequestData(newRequestData);
        saaSShApiServicePublicOperate.multipleCallStrategyItem(testDataBean,pubCommonAction,caseResultCheckMapper,variableDataBean,propertyUtil.getproperValue("multipleCallStrategyEditItem.uri"));
    }

    /**策略管理-多轮策略-查询轮次**/
    public void multipleCallStrategyGetItemApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("multipleCallStrategyGetItem.uri"));
    }

    /**策略管理-多轮策略-重排轮次**/
    public void multipleCallStrategyConfigItemApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        List<String> codeList = new ArrayList<>();
        codeList.add(requestUtils.getSaveLinked().get("code").toString());
        jsonObject.put("strategyItemCodes",codeList);
        testDataBean.setRequestData(jsonObject.toString());
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("multipleCallStrategyConfigItem.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**策略管理-多轮策略-删除轮次**/
    public void multipleCallStrategyDeleteItemApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("multipleCallStrategyDeleteItem.uri"));
        String multipleCallStrategyTableConfig = caseResultCheckMapper.getMultipleCallStrategyConfig(variableDataBean.getMultipleDialStrategyName());
        if("[]".equals(multipleCallStrategyTableConfig)){
            MyLogger.myResponseLogger("【"+testDataBean.getApiDescription()+"】接口的轮次配置字段值为空，删除轮次成功");
        }
        else{
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口的轮次配置字段值仍然存在，删除轮次配置失败");
        }
    }

    /**话术管理-添加话术**/
    public void addDiscourseApi(TestDataBean testDataBean){
        String discourseName;
        if(!StringUtils.isNotBlank(testDataBean.getAssist())){
            //随机生成话术名称
            discourseName = "自动"+RandomUtil.randomStr(3);
            variableDataBean.setDiscourseName(discourseName);
            variableMap.put("discourseName",discourseName);
        }
        else{
            discourseName = testDataBean.getAssist();
        }
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addDiscourse.uri"));
        if(!testDataBean.getApiDescription().contains("异常场景")){
            int status = caseResultCheckMapper.getDiscourseCount(discourseName);
            CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),status,1);
        }
    }

    /**话术管理-编辑话术**/
    public void updateDiscourseApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("updateDiscourse.uri"));
        int templateVerbalSectionTableCount = caseResultCheckMapper.getDiscourseCount(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoal(templateVerbalSectionTableCount,testDataBean.getApiDescription());
    }

    /**话术管理-话术列表**/
    public void discourseManagerListApi(TestDataBean testDataBean){
        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("discourseManagerList.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**话术管理-话术详情**/
    public void discourseDetailApi(TestDataBean testDataBean){
        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("discourseDetail.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
        //检查每个数组里的每个话术内容是否存在
        JSONObject jsonObject = JSONObject.parseObject(responseBody);
        JSONObject data = jsonObject.getJSONObject("data");
        JSONArray verbalItems = data.getJSONArray("verbalItems");
        for(int i =0;i<verbalItems.size();i++){
            JSONObject o = (JSONObject)verbalItems.get(i);
            if(!StringUtils.isNotBlank(o.getString("context"))){
                CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口响应报文里的话术内容丢失");
            }
        }
    }

    /**话术管理-上传录音文件**/
    public void uploadAudioFileApi(TestDataBean testDataBean){
        JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        jsonObject.put("file",PlatformConstants.RECORDING_FILE);
        testDataBean.setRequestData(jsonObject.toString());
        testDataBean.setUrl(propertyUtil.getproperValue("uploadAudioFile.uri"));
        String body = requestUtils.fileUpLoad(testDataBean);
        CheckPointUtils.checkResponseCodeMsg(body,testDataBean.getApiDescription());
    }

    /**话术管理-播放录音文件**/
    public void playAudioApi(TestDataBean testDataBean){
        testDataBean.setUrl(propertyUtil.getproperValue("playAudio.uri"));
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
        long contentLength = closeableHttpResponse.getEntity().getContentLength();
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),statusCode,200);
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),contentLength,-1L);
    }

    /**话术管理-话术试听**/
    public void discourseAuditionApi(TestDataBean testDataBean){
        testDataBean.setUrl(propertyUtil.getproperValue("discourseAudition.uri"));
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),statusCode,200);
    }

    /**话术管理-列表播放话术**/
    public void listPlayDiscourseApi(TestDataBean testDataBean){
        testDataBean.setUrl(propertyUtil.getproperValue("listPlayDiscourse.uri"));
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),statusCode,200);
    }

    /**话术流程-添加辅助流程**/
    public void addAssistFlowApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addAssistFlow.uri"));
        int templateFlowGroupTableCount = caseResultCheckMapper.getFlowGroupCount(variableDataBean.getAssistFlowName());
        pubCommonAction.checkPointFromDBQueryGoal(templateFlowGroupTableCount,testDataBean.getApiDescription());
    }

    /**话术流程-辅助流程列表**/
    public void assistFlowListApi(TestDataBean testDataBean){
        Map<String, Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean, variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("assistFlowList.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**话术流程-添加用户意图**/
    public void addUserIntentionApi(TestDataBean testDataBean){
        pubCommonAction.jsonStringAsValue(testDataBean);
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addUserIntention.uri"));
        int templateIntentionTableCount = caseResultCheckMapper.getIntentionCount(variableDataBean.getUserIntention());
        pubCommonAction.checkPointFromDBQueryGoal(templateIntentionTableCount,testDataBean.getApiDescription());
    }

    /**话术流程-用户意图列表**/
    public void userIntentionListApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean,variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("userIntention.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**话术流程-分支意图详情**/
    public void branchIntentionDetailApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean,variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("branchIntentionDetail.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**话术流程-获取流程树**/
    public void getFlowTreeApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean,variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("getFlowTree.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**话术流程-主流程根节点添加默认的4个意图**/
    public void defaultFourIntentionApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = pubCommonAction.requestDataCheckInitialize(testDataBean,variableMap);
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("defaultFourIntention.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**话术流程-新增或编辑分支意图**/
    public void addOrEditBranchIntentionApi(TestDataBean testDataBean){
        pubCommonAction.jsonStringAsValue(testDataBean);
        String assistField = testDataBean.getAssist();
        String randomBranchIntentionName = "自动"+RandomUtil.randomStr(3)+"意图";
        if(null != assistField &&! "".equals(assistField)){
            //随机生成分支意图名称
            String newRequestParam = testDataBean.getRequestData().replace("分支意图名", randomBranchIntentionName);
            testDataBean.setRequestData(newRequestParam);
        }
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addOrEditNodeIntention.uri"));
        String branchIntentionName = null == assistField||"".equals(assistField)?variableDataBean.getBranchIntentionName():randomBranchIntentionName;
        int templateIntentionTableCount = caseResultCheckMapper.getIntentionCount(branchIntentionName);
        pubCommonAction.checkPointFromDBQueryGoal(templateIntentionTableCount,testDataBean.getApiDescription());
    }

    /**话术流程-新增或编辑节点**/
    public void addOrEditNodeApi(TestDataBean testDataBean){
        String assistField = testDataBean.getAssist();
        String randomFlowNodeName = "自动"+RandomUtil.randomStr(3)+"节点";
        if(null != assistField &&! "".equals(assistField)){
            //随机生成流程节点名称
            String newRequestParam = testDataBean.getRequestData().replace("流程节点名", randomFlowNodeName);
            testDataBean.setRequestData(newRequestParam);
        }
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addOrEditNodeDetail.uri"));
        String flowNodeName;
        if(testDataBean.getApiDescription().contains("话术流程-编辑主流程根节点")){
            flowNodeName = variableDataBean.getMainFlowOpenRemarks();
        }
        else if(testDataBean.getApiDescription().contains("话术流程-编辑辅助流程根节点")){
            flowNodeName = variableDataBean.getAssistFlowOpenRemarks();
        }
        else {
            flowNodeName = null == assistField||"".equals(assistField)?variableDataBean.getFlowNodeName():randomFlowNodeName;
        }
        int countDB = caseResultCheckMapper.getGoalFlowNodeData(flowNodeName);
        pubCommonAction.checkPointFromDBQueryGoal(countDB,testDataBean.getApiDescription());
    }

    /**话术流程-流程节点详情**/
    public void getNodeDetailApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("nodeDetail.uri"));
    }
    /**话术流程-删除分支意图**/
    public void deleteBranchIntentionApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteBranchIntention.uri"));
        int countDB = caseResultCheckMapper.getIntentionCount(variableDataBean.getBranchIntentionName());
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(countDB,testDataBean.getApiDescription());
    }
    /**话术流程-删除流程节点**/
    public void deleteFlowNodeApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteFlowNode.uri"));
        int countDB = caseResultCheckMapper.getFlowGroupCountFromId(requestUtils.getSaveLinked().get(testDataBean.getAssist()).toString());
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(countDB,testDataBean.getApiDescription());
    }
    /**话术流程-删除用户意图**/
    public void deleteCustomerIntentionApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteCustomerIntention.uri"));
        int countDB = caseResultCheckMapper.getTemplateIntentionRankCount(variableDataBean.getUserIntention());
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(countDB,testDataBean.getApiDescription());
    }
    /**话术流程-删除辅助流程**/
    public void deleteAssistFlowApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("deleteAssistFlow.uri"));
        int countDB = caseResultCheckMapper.getFlowGroupCountFromId(requestUtils.getSaveLinked().get(testDataBean.getAssist()).toString());
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(countDB,testDataBean.getApiDescription());
    }

    /**添加短信签名**/
    public void addSmsSignApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addSmsSign.uri"));
    }

    /**查询短信签名**/
    public void querySmsSignApi(TestDataBean testDataBean){

        pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("querySmsSign.uri"));
    }

    /**短信签名列表**/
    public void smsSignListApi(TestDataBean testDataBean){

        pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("smsSignList.uri"));
    }

    /**短信模板列表**/
    public void smsTemplateListApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.getPubActionProcess(testDataBean, propertyUtil.getproperValue("smsTemplateList.uri"));
        JSONObject bodyJsonObject = JSONObject.parseObject(responseBody);
        JSONArray dataArray = bodyJsonObject.getJSONArray("data");
        int i = 0;
        for(;i<dataArray.size();i++){
            JSONObject dataJsonObject = (JSONObject)dataArray.get(i);
            if(dataJsonObject.getString("name").equals(variableDataBean.getSmsTemplateName())){
                requestUtils.getSaveLinked().put("smsTemplateId",dataJsonObject.getString("id"));
                break;
            }
        }
        if(i >= dataArray.size()){
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口没有新增的短信模板记录");
        }
    }

    /**短信通道列表**/
    public void smsChannelListApi(TestDataBean testDataBean){
        pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("smsChannelList.uri"));
    }

    /**新增短信模板**/
    public void addSmsTemplateApi(TestDataBean testDataBean){

        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addSmsTemplate.uri"));
    }

    /**修改短信模板**/
    public void modifySmstemplateApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("modifySmsTemplate.uri"));
    }

    /**查询短信模板**/
    public void querySmsTemplateApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("querySmsTemplate.uri"));
    }

    /**短信监控-导出短信监控**/
    public void exportSmsMonitorApi(TestDataBean testDataBean){
        testDataBean.setUrl(propertyUtil.getproperValue("exportSmsMonitor.uri"));
        pubCommonAction.fileStreamProcess(testDataBean);
    }
    /**短信监控-短信监控列表**/
    public void smsMonitorListApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("smsMonitorList.uri"));
    }
    /**短信监控-短信监控页面的短信发送详情**/
    public void smsMonitorSendDetailApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("smsMonitorSendDetail.uri"));
    }



    /**全局设置- 新增、修改 未识别设置**/
    public void addModifyUnidentifiedConfigApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addModifyUnidentifiedConfig.uri"));
    }

    /**全局设置- 查看未识别设置**/
    public void lookUnidentifiedConfigApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("lookUnidentifiedConfig.uri"));
        JSONObject jsonObject = JSONObject.parseObject(responseBody);
        JSONArray dataJsonArray = jsonObject.getJSONArray("data");
        for(int i =0;i<dataJsonArray.size();i++){
            JSONObject jsonObject1 = dataJsonArray.getJSONObject(i);
            String id = jsonObject1.getString("id");
            String unidentifiedCount = jsonObject1.getString("unidentifiedCount");
            String verbalSectionId = jsonObject1.getString("verbalSectionId");
            String actionType = jsonObject1.getString("actionType");
            String agentGroupId = jsonObject1.getString("agentGroupId");
            String callTemplateId = jsonObject1.getString("callTemplateId");
            if(StringUtils.isNotBlank(id)&& StringUtils.isNotBlank(unidentifiedCount)&& StringUtils.isNotBlank(verbalSectionId) &&
                    StringUtils.isNotBlank(actionType)&&StringUtils.isNotBlank(agentGroupId)&&StringUtils.isNotBlank(callTemplateId)){
                MyLogger.myResponseLogger("【"+testDataBean+"】接口断言body字段不为空正常");
            }
            else{
                MyLogger.myResponseLogger("【"+testDataBean+"】接口断言body里的data数组里的对象存在字段为空，有问题的哦");
            }
        }
    }

    /**全局设置- 修改打断设置**/
    public void modifyInterruptConfigApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("modifyInterruptConfig.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**全局设置- 查看打断设置**/
    public void lookInterruptConfigApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("lookInterruptConfig.uri"));
    }

    /**全局设置- 新增、修改超时设置**/
    public void addModifyTimeoutConfigApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addModifyTimeoutConfig.uri"));
    }

    /**全局设置- 查看超时设置**/
    public void lookTimeoutConfigApi(TestDataBean testDataBean){
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("lookTimeoutConfig.uri"));
        JSONObject jsonObject = JSONObject.parseObject(responseBody);
        JSONArray dataJsonArray = jsonObject.getJSONArray("data");
        for(int i =0;i<dataJsonArray.size();i++){
            JSONObject jsonObject1 = dataJsonArray.getJSONObject(i);
            String id = jsonObject1.getString("id");
            String timeoutLimit = jsonObject1.getString("timeoutLimit");
            String timeoutCount = jsonObject1.getString("timeoutCount");
            String verbalSectionId = jsonObject1.getString("verbalSectionId");
            String actionType = jsonObject1.getString("actionType");
            String callTemplateId = jsonObject1.getString("callTemplateId");
            if(StringUtils.isNotBlank(id)&& StringUtils.isNotBlank(timeoutLimit)&& StringUtils.isNotBlank(verbalSectionId) &&
                    StringUtils.isNotBlank(actionType)&&StringUtils.isNotBlank(timeoutCount)&&StringUtils.isNotBlank(callTemplateId)){
                MyLogger.myResponseLogger("【"+testDataBean+"】接口断言body字段不为空正常");
            }
            else{
                MyLogger.myResponseLogger("【"+testDataBean+"】接口断言body里的data数组里的对象存在字段为空，有问题的哦");
            }
        }
    }

    /**全局设置- 修改无法打断节点配置**/
    public void modifyUnableInterruptConfigApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("modifyUnableInterruptConfig.uri"));
    }

    /**全局设置- 查看无法打断的节点设置**/
    public void lookUnableInterruptConfigApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("lookUnableInterruptConfig.uri"));
    }

    /**全局设置- 查看TTS设置**/
    public void lookTTSConfigApi(TestDataBean testDataBean){
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("lookTTSConfig.uri"));
        String data = JSONObject.parseObject(body).getJSONArray("data").getJSONObject(0).toJSONString();
        requestUtils.getSaveLinked().put("defaultTTS",data);
    }

    /**全局设置- 新增、修改TTS**/
    public void addModifyTTSConfigApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String defaultTTS = requestUtils.getSaveLinked().get("defaultTTS").toString();
        String newRequestParam = testDataBean.getRequestData().replace("默认TTS", defaultTTS);
        testDataBean.setRequestData(newRequestParam);
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addModifyTTSConfig.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }

    /**全局设置- 开关打断设置**/
    public void switchInterruptStatusApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("switchInterruptStatus.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**全局设置- 开关不可打断设置**/
    public void switchUnableInterruptStatusApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String responseBody = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("switchUnableInterruptStatus.uri"));
        CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
    }

    /**全局设置- 获取模板下的所有话术**/
    public void getTemplateAllDiscoursesApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("discourseTemplateAllDiscourse.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }

    /**全局设置- TTS发音人列表查询**/
    public void getOrgTtsInfoApi(TestDataBean testDataBean){
        String apiDescription = testDataBean.getApiDescription();
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("getOrgTtsInfo.uri"));
        JSONArray ttsVoiceListDB = JSONArray.parseArray(caseResultCheckMapper.getTTSVoiceList());
        JSONArray ttsVoiceList = JSONArray.parseArray(JsonPathRead.readJson(body, "$.data.supplier"));
        for(int i = 0;i<ttsVoiceList.size();i++){
           Map<String,String> mapActual = (Map)ttsVoiceList.getJSONObject(i);
           Map<String,String> mapDB = (Map) ttsVoiceListDB.getJSONObject(i);
           for(Map.Entry map :mapActual.entrySet()){
               CheckPointUtils.myAssertEquals(apiDescription,map.getValue(),mapDB.get(map.getKey()));
           }
        }
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),ttsVoiceList,ttsVoiceListDB);
    }

    /**全局设置- TTS试听**/
    public void TTSAuditionApi(TestDataBean testDataBean){
        testDataBean.setUrl(propertyUtil.getproperValue("TTSAudition.uri"));
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        int headCode = closeableHttpResponse.getStatusLine().getStatusCode();
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),headCode,200);
    }

    /**全局设置-TTS选择条件数据**/
    public void TTSSelectDataApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("TTSSelectData.uri"));
    }

    /**短信任务-下载电话短信模板**/
    public void downLoadSmsExcelApi(TestDataBean testDataBean){
        testDataBean.setUrl(propertyUtil.getproperValue("downLoadSmsExcel.uri"));
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        int headCode = closeableHttpResponse.getStatusLine().getStatusCode();
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),headCode,200);
        HttpEntity entity = closeableHttpResponse.getEntity();
        String contentType = entity.getContentType().toString();
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),contentType,testDataBean.getChecks());
    }
    /**短信任务-客户信息上传**/
    public void customerInfoUploadApi(TestDataBean testDataBean){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("file",PlatformConstants.CUSTOMER_INFO);
        testDataBean.setRequestData(jsonObject.toString());
        testDataBean.setUrl(propertyUtil.getproperValue("customerInfoUpload.uri"));
        String body = requestUtils.fileUpLoad(testDataBean);
        CheckPointUtils.checkResponseCodeMsg(body,testDataBean.getApiDescription());
    }
    /**短息任务-新增短信任务**/
    public void addSmsTaskApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        if(testDataBean.getApiDescription().contains("定时发送")){
            testDataBean.setRequestData(testDataBean.getRequestData().replace("定时发送", DateTimeUtil.timeLateTProcess()));
        }
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addSmsTask.uri"));
        String assistField = testDataBean.getAssist();
        String smsTaskName = null == assistField || assistField.equals("")? variableDataBean.getSmsTaskName():assistField;
        int smsTaskDB = caseResultCheckMapper.getSmsTask(smsTaskName);
        pubCommonAction.checkPointFromDBQueryGoal(smsTaskDB,testDataBean.getApiDescription());
    }
    /**短信任务-查询短信任务**/
    public void searchSmsTaskApi(TestDataBean testDataBean){
        String apiName = testDataBean.getApiDescription();
        if(apiName.contains("发送方式")){
            int countDB = apiName.contains("定时发送方式")?caseResultCheckMapper.getSendTypeSmsTask(1):caseResultCheckMapper.getSendTypeSmsTask(0);
            String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("searchSmsTask.uri"));
            int count = Integer.parseInt(JsonPathRead.readJson(body, "$.data.records.length()"));
            CheckPointUtils.myAssertEquals(apiName,count,countDB);
        }
        else if(apiName.contains("名称不存在查询")){
            Map<String, Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
            String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("searchSmsTask.uri"));
            CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
        }
        else if(apiName.contains("时间查询")){

        }
        else {
            Map<String, Object> checkMap = pubCommonAction.requestDataInitialize(testDataBean, variableMap);
            String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("searchSmsTask.uri"));
            CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
            //校验body里的content字段是否为空
            String smsTemplateContent = JsonPathRead.readJson(body,"$.data.records[0].content",testDataBean.getApiDescription());
            if(StringUtils.isNotBlank(smsTemplateContent)){
                MyLogger.myResponseLogger("【"+testDataBean.getApiDescription()+"】接口的body里的content字段校验通过");
            }
            else{
                CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口的body里的content字段校验失败，content字段为空");
            }
        }
    }
    /**短信任务-查看短信任务详情**/
    public void lookSmsTaskDetailApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("lookSmsTaskDetail.uri"));
    }
    /**短信任务-导出短信任务结果**/
    public void exportSmsTaskResultApi(TestDataBean testDataBean){
        pubCommonAction.requestParamInitialize(testDataBean,variableMap);
        testDataBean.setUrl(propertyUtil.getproperValue("exportSmsTaskResult.uri"));
        pubCommonAction.fileStreamProcess(testDataBean);
    }
    /**短信任务-删除短信任务**/
    public void deleteSmsTaskApi(TestDataBean testDataBean){
        pubCommonAction.deletePubActionProcess(testDataBean,propertyUtil.getproperValue("deleteSmsTask.uri"));
        int smsTaskDB = caseResultCheckMapper.getSmsTask(variableDataBean.getSmsTaskName());
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(smsTaskDB,testDataBean.getApiDescription());
    }

    /**NLU-模型训练**/
    public void modelTrainingApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("modelTrain.uri"));
        //从数据库通过话术模板id找到模型版本和模板编码
        String discourseTemplateId = requestUtils.getSaveLinked().get("discourseTemplateId").toString();
        SleepUtil.sleep();
        String version = caseResultCheckMapper.getNLUVersionData(discourseTemplateId);
        List<String> codeList = caseResultCheckMapper.getNLUCodeData( discourseTemplateId);
        if(!StringUtils.isNotBlank(version) && codeList.size() ==0){
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口在数据库中找不到生成的数据即code和version");
        }
        requestUtils.getSaveLinked().put("modelVersion",version);
        requestUtils.getSaveLinked().put("modelCode",codeList);
        // 查看数据库模型训练状态
        List<Integer> statusList ;
        List<Integer> trainingSuccess = new ArrayList<>();
        int i =0;
        for( ; i <600; i++ ){
            SleepUtil.sleep(3);
            statusList = caseResultCheckMapper.getNLUStatusData(discourseTemplateId);
            for(int j = 0 ; j<statusList.size(); j++){
                if(0 == statusList.get(j)){
                    MyLogger.myResponseLogger("第 "+j+" 个模型训练完成咯!");
                    trainingSuccess.add(j);
                }
                else {
                    MyLogger.myResponseLogger("第 "+j+" 个模型训练并未完成......等待3秒后继续查训练状态");
                    trainingSuccess.clear();
                    break;
                }
            }
            if(trainingSuccess.size() == statusList.size()){
                MyLogger.myResponseLogger("模型训练完成咯!准备进行模型加载......");
                break;
            }
        }
        if(i >= 300){
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口训练失败,训练了30分钟");
        }
    }

    /**NLU-模型加载**/
    public void modelLoadApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("modelLoad.uri"));
    }

    /**NLU-模型预测**/
    public void modelForecastApi(TestDataBean testDataBean){
        JSONObject jsonObject =JSONObject.parseObject( testDataBean.getRequestData());
        jsonObject.put("modelCodes",requestUtils.getSaveLinked().get("modelCode"));
        testDataBean.setRequestData(jsonObject.toJSONString());
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("modelForecast.uri"));
    }

    /**NLU-模型状态**/
    public void modelStatusApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("modelStatus.uri"));
    }

    /**NLU-模型卸载**/
    public void modelUninstallApi(TestDataBean testDataBean){

        System.out.println("模型卸载暂时不做。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。");
    }

    /**
     * 机构管理-添加机构
     */
    public void addOrganizationApi(TestDataBean testDataBean) {
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addOrganization.uri"));
        int countDB = caseResultCheckMapper.getOrganizationTableData(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoal(countDB,testDataBean.getApiDescription());
        String organizationId = caseResultCheckMapper.getOrganizationTableIdData(testDataBean.getAssist());
        requestUtils.getSaveLinked().put("handleOrganizeId",organizationId);
    }
    /**机构管理-获取机构树**/
    public void organizationTreeApi(TestDataBean testDataBean){
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("organizationTree.uri"));
        //获取望潮机构下面的所有子机构个数
        List<String> organizationNames = caseResultCheckMapper.getOrganizationTableOrgName();
        int childrenOrganizations = Integer.parseInt(JsonPathRead.readJson(body,"$.data.organizeTrees[0].children.length()"));
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),organizationNames.size(),childrenOrganizations);
        //遍历望潮机构下的子机构名称是否正确
        JSONArray children = JSONObject.parseObject(body).getJSONObject("data").getJSONArray("organizeTrees").getJSONObject(0).getJSONArray("children");
        for(int i = 0; i<children.size();i++){
            String name = children.getJSONObject(i).getString("name");
            CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),name,organizationNames.get(i));
        }
    }
    /**机构管理-获取机构详情**/
    public void organizationDetailApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("organizationDetail.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }
    /**机构管理-获取所有机构信息**/
    public void organizationAllInfoApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean, propertyUtil.getproperValue("organizationAllInfo.uri"));
    }

    /**机构管理-更新机构信息**/
    public void organizationUpateApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("updateOrganization.uri"));
        int countDB = caseResultCheckMapper.getOrganizationTableData(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoal(countDB,testDataBean.getApiDescription());
    }
    /**机构管理-添加用户**/
    public void addUserApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addUser.uri"));
        int countDB = caseResultCheckMapper.getUserTableData(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoal(countDB,testDataBean.getApiDescription());
        String userId = caseResultCheckMapper.getUserTableIdData(testDataBean.getAssist());
        requestUtils.getSaveLinked().put("handleUserId",userId);
    }

    /**机构管理-修改用户**/
    public void userUpdateApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("userUpdate.uri"));
        int countDB = caseResultCheckMapper.getUserTableData(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoal(countDB,testDataBean.getApiDescription());

    }
    /**机构管理-用户详情**/
    public void userDetailApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("userDetail.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }
    /**机构管理-删除机构**/
    public void organizationDeleteApi(TestDataBean testDataBean){
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("organizationDelete.uri"));
        int countDB = caseResultCheckMapper.getOrganizationTableData(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoalIsDelete(countDB,testDataBean.getApiDescription());
    }

    ///////////////////////////////////////////////////////////////
    /**菜单列表**/
    public void menuListApi(TestDataBean testDataBean){
        List<String> menuId = new ArrayList<>();
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("menuList.uri"));
        JSONArray jsonArrayData = JSONArray.parseArray(JsonPathRead.readJson(body,"$.data"));
        for(int i = 0;i<jsonArrayData.size();i++){
            String id = jsonArrayData.getJSONObject(i).getString("id");
            menuId.add(id);
        }
        requestUtils.getSaveLinked().put("menuIds",menuId);
    }
    /**角色管理-添加角色**/
    public void addRoleApi(TestDataBean testDataBean){
        JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        jsonObject.put("permissionIds",requestUtils.getSaveLinked().get("menuIds"));
        testDataBean.setRequestData(jsonObject.toJSONString());
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("addRole.uri"));
        String handleRoleId = caseResultCheckMapper.getRoleTableIdData(testDataBean.getAssist());
        requestUtils.getSaveLinked().put("handleRoleId",handleRoleId);
    }
    /**角色管理-修改角色**/
    public void updateRoleApi(TestDataBean testDataBean){
        JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        jsonObject.put("permissionIds",requestUtils.getSaveLinked().get("menuIds"));
        testDataBean.setRequestData(jsonObject.toJSONString());
        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("updateRole.uri"));
        int countDB = caseResultCheckMapper.getRoleTableDataCount(testDataBean.getAssist());
        pubCommonAction.checkPointFromDBQueryGoal(countDB,testDataBean.getApiDescription());
    }
    /**角色管理-角色详情**/
    public void roleDetailApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("roleDetail.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
        List menuIds = (List)requestUtils.getSaveLinked().get("menuIds");
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),Integer.parseInt(JsonPathRead.readJson(body,"$.data.menuTrees.length()")),menuIds.size());
    }
    /**角色管理-角色分页查询**/
    public void rolePageApi(TestDataBean testDataBean){
        int countDB = caseResultCheckMapper.getRoleTableDataCountAll();
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("rolePage.uri"));
        int count = Integer.parseInt(JsonPathRead.readJson(body, testDataBean.getChecks()));
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),count,countDB);
        JSONArray jsonArray = JSONObject.parseObject(body).getJSONObject("data").getJSONArray("records");
        roleCheck(jsonArray,testDataBean.getApiDescription());

    }
    /**角色管理-所有角色列表**/
    public void allRoleListApi(TestDataBean testDataBean){
        int countDB = caseResultCheckMapper.getRoleTableDataCountAll();
        String body = pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("roleList.uri"));
        int count = Integer.parseInt(JsonPathRead.readJson(body, testDataBean.getChecks()));
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),count,countDB);
        JSONArray jsonArray = JSONObject.parseObject(body).getJSONArray("data");
        roleCheck(jsonArray,testDataBean.getApiDescription());
    }

    private void roleCheck(JSONArray jsonArray,String apiDescription){
        for(int i = 0 ;i<jsonArray.size();i++){
            String name = jsonArray.getJSONObject(i).getString("name");
            if(!StringUtils.isNotBlank(name)){
                CheckPointUtils.myAssertFalse("【"+apiDescription+"】接口菜单列表的名称有为空的");
            }
        }
        logger.info("【"+apiDescription+"】接口菜单列表的名称都存在");
    }

    /**个人中心-用户详情**/
    public void personCenterUserDetailApi(TestDataBean testDataBean){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        String body = pubCommonAction.getPubActionProcess(testDataBean,propertyUtil.getproperValue("personCenterUserDetail.uri"));
        CheckPointUtils.checkPoints(body,checkMap,testDataBean.getApiDescription());
    }
    /**个人中心-修改密码**/
    public void personCenterModifyPwdApi(TestDataBean testDataBean){

        pubCommonAction.postPubActionProcess(testDataBean,propertyUtil.getproperValue("personCenterModifyPwd.uri"));
    }
}
