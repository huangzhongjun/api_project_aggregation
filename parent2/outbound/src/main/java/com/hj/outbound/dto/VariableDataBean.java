package com.hj.outbound.dto;

import lombok.Data;

/**
 * 变量
 */
@Data
public class VariableDataBean {

    /**开放性话术名称(标题)**/
    String openDiscourseName;

    /**行业话术名称**/
    String industryDiscourseName;

    /**多轮拨打计划名称**/
    String multipleDialPlanName;

    /**单轮拨打计划名称**/
    String dialPlanName;

    /**行业知识库名称**/
    String industryKnowledgeName;

    /**知识库名称**/
    String knowledgeName;

    /**坐席组称**/
    String SeatGroupName;

    /**客户姓名**/
    String CustomerName;

    /**客户手机号**/
    String phoneNum;

    /**性别**/
    String sex;

    /**批次号**/
    String batchNum;

    /**话术模板名称**/
    String discourseTemplateName;

    /**话术名称**/
    String discourseName;

    /**话术内容**/
    String discourseContent;

    /**意向等级名称**/
    String intentionRankName;

    /**客户字典**/
    String customerDictName;

    /**拨打策略名称**/
    String dialStrategyName;

    /**多轮拨打策略名称**/
    String multipleDialStrategyName;

    /**辅助流程名称**/
    String assistFlowName;

    /**流程节点名称**/
    String flowNodeName;

    /**用户意图**/
    String userIntention;

    /**分支意图名称**/
    String branchIntentionName;

    /**短信模板名称**/
    String smsTemplateName;

    /**短信任务名称**/
    String smsTaskName;

    /**主流程开场白**/
    String mainFlowOpenRemarks;

    /**辅助流程开场白**/
    String assistFlowOpenRemarks;
}
