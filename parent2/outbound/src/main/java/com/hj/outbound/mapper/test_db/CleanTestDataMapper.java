package com.hj.outbound.mapper.test_db;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Qualifier;


@Qualifier("devSqlSessionFactoryRef")
public interface CleanTestDataMapper {

    @Delete("delete from ${tableName} where name like '自动%'")
    void deleteNameTestData(@Param("tableName") String tableName);

    @Delete("delete from ${tableName} where customer_name like '自动%'")
    void deleteCustomerNameTestData(@Param("tableName") String tableName);

    @Delete("delete from ${tableName} where sign like '自动%'")
    void deleteSignTestData(@Param("tableName") String tableName);

    @Delete("delete from ${tableName} where customer_info like '%自动%'")
    void deleteCustomerInfoTestData(@Param("tableName") String tableName);

    @Delete("delete from ${tableName} where context like '自动%'")
    void deleteContextTestData(@Param("tableName") String tableName);

    @Delete("delete from user where nickname like '自动%'")
    void deleteUserData();

    @Delete("delete from dialogue_record where call_plan_id NOT IN (select id from call_plan)")
    void deleteDialogueRecordData();

    @Delete("delete from published_call_template where flow_config like '%自动%' ")
    void deletePublishCallTemplateData();

}
