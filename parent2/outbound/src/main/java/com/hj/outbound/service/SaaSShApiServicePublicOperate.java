package com.hj.outbound.service;


import com.hj.common.dto.TestDataBean;
import com.hj.common.utils.CheckPointUtils;
import com.hj.common.utils.DateTimeUtil;
import com.hj.common.utils.MyLogger;
import com.hj.outbound.common.PubCommonAction;
import com.hj.outbound.dto.VariableDataBean;
import com.hj.outbound.mapper.test_db.CaseResultCheckMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 针对SaaSShApiService类的相同操作处理
 */
@Component
public class SaaSShApiServicePublicOperate{

    /**
     * 多轮策略-添加、编辑 轮次操作
     * @param testDataBean
     * @param pubCommonAction
     * @param caseResultCheckMapper
     * @param variableDataBean
     */
    public void multipleCallStrategyItem(TestDataBean testDataBean, PubCommonAction pubCommonAction, CaseResultCheckMapper caseResultCheckMapper, VariableDataBean variableDataBean, String uri){

        String requestData = testDataBean.getRequestData();
        requestData = requestData.replace("拨打时间", DateTimeUtil.timeAddMinute("",5));
        testDataBean.setRequestData(requestData);
        pubCommonAction.postPubActionProcess(testDataBean,uri);
        String multipleCallStrategyTableConfig = caseResultCheckMapper.getMultipleCallStrategyConfig(variableDataBean.getMultipleDialStrategyName());
        if(!StringUtils.isNotBlank(multipleCallStrategyTableConfig)){
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口的轮次配置字段值为空");
        }
        else{
            MyLogger.myResponseLogger("【"+testDataBean.getApiDescription()+"】接口的轮次配置字段值正常");
        }
    }
}
