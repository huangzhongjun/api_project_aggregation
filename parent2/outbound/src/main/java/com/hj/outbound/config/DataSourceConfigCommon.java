package com.hj.outbound.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;

import javax.sql.DataSource;

public class DataSourceConfigCommon {

    SqlSessionFactoryBean bean = new SqlSessionFactoryBean();

    public SqlSessionFactory dataSourceConfig(DataSource dataSource) throws  Exception{

        bean.setDataSource(dataSource);
        org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration();
        config.setMapUnderscoreToCamelCase(true);
        bean.setConfiguration(config);
        return bean.getObject();
    }
}
