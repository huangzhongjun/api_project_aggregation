package com.hj.outbound.common;

import com.hj.outbound.cases.CaseBase;
import com.hj.outbound.mapper.test_db.CleanTestDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.testng.annotations.Test;

public class CleanTestData extends CaseBase {

    private Logger logger = LoggerFactory.getLogger(CleanTestData.class);

    @Autowired
    CleanTestDataMapper cleanTestDataMapper;

    @Test
    public void cleanTestData(){
        logger.info("开始清除测试数据");

        String tableNames[]={"open_verbal","agent_group","call_strategy","call_template","industry_knowledge","industry_knowledge_item",
                            "template_intention_rank","template_intention","template_customer_dict","template_knowledge","organization",
                            "template_verbal_section","template_flow_group","template_flow","sms_template","multiple_call_strategy","call_plan","sms_send_batch",
                            "role"};
        for(String tableName:tableNames){
            cleanTestDataMapper.deleteNameTestData(tableName);
        }
        cleanTestDataMapper.deleteCustomerNameTestData("crm_customer");
        cleanTestDataMapper.deleteSignTestData("sms_sign");
        cleanTestDataMapper.deleteContextTestData("template_verbal_item");
        cleanTestDataMapper.deleteCustomerInfoTestData("phonebook_number");
        cleanTestDataMapper.deleteCustomerInfoTestData("sms_send_batch_detail");
        cleanTestDataMapper.deleteUserData();
        cleanTestDataMapper.deleteDialogueRecordData();
        cleanTestDataMapper.deletePublishCallTemplateData();
        logger.info("清除测试数据完毕");
    }
}
