package com.hj.outbound.cases;

import com.hj.common.base.TestNgBase;
import com.hj.outbound.OutboundApplication;
import com.hj.outbound.common.PresetTestData;
import com.hj.outbound.dto.VariableDataBean;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SpringBootTest(classes = {OutboundApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class CaseBase extends TestNgBase {

    public VariableDataBean variableDataBean = new VariableDataBean();

    public Map<String ,Object> variableMap = new ConcurrentHashMap<>();

    public PresetTestData presetTestData = new PresetTestData();

    @BeforeClass
    public void beforeTest(){

        presetTestData.presetTestData(variableDataBean,variableMap);
    }

    @BeforeSuite
    public void testEnvIp(){
        System.setProperty("Env","test");
        System.setProperty("pip", "http://10.253.45.91:8095");
        System.setProperty("ip", "http://10.253.45.91:8095/bsc");
    }
}
