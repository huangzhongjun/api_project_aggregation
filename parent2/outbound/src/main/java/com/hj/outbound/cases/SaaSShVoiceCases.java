package com.hj.outbound.cases;

import com.hj.common.base.TestNgBase;
import com.hj.common.dto.TestDataBean;
import com.hj.outbound.dataprovider.SaaSShVoiceDataProvider;
import com.hj.outbound.service.SaaSShCombinationApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;


/**
 * SaaS 慧声语音系统
 *
 * @author huangjun
 *
 */

public class SaaSShVoiceCases extends CaseBase {

    final String addEditDeleteOpenDiscourseCase ="开放性话术添加-编辑-删除";
    final String exportOpenDiscourseCase ="开放性话术添加-导出";
    final String batchCheckOpenDiscourseCase ="开放性话术批量确认";
    final String addDeleteSeatGroupCase ="坐席组添加-删除";
    final String artificialSeatCase = "人工坐席功能";
    final String industryKnowledgeBaseCase ="行业知识库模块";
    final String customersManageModuleCase ="客户管理-客户名单管理模块";
    final String addDialTaskCase = "客户管理-添加一键外呼(单策略-sms关闭)";
    final String discourseTemplateModuleCase ="话术模板功能";
    final String intentionRankModuleCase ="意向等级模块";
    final String discourseDictModuleCase ="话术词典-新增-列表-启用";
    final String knowledgeCase ="自定义知识库模块-新增-详情-列表-修改-删除";
    final String importKnowledgeCase = "导入知识库(行业性与开放性)";
    final String industryAndDiscourseClassCase ="行业和话术分类";
    final String callStrategyManageCase ="策略管理模块";
    final String deleteCallStrategyCase ="单轮拨打策略删除";
    final String deleteMultipleCallStrategyCase ="多轮拨打策略删除";
    final String discourseManageCase ="话术管理";
    final String discourseFlowCase ="话术流程-列表-详情-删除操作";
    final String discourseFlowDeleteAssistFlowCase="话术流程-删除辅助流程(存在意图和节点)";
    final String dialPlanPartCase = "拨打计划模块-暂停-继续-二次呼叫等操作";
    final String dialPlanPart2Case = "拨打计划模块-导出-详情-查看-下载-删除等操作";
    final String smsSingTemplateCase = "短信签名短信模板功能";
    final String smsTaskCase = "短信任务功能";
    final String smsMonitorCase = "短信监控功能";
    final String callRecordsCase = "通话记录功能";
    final String globalSetCase = "全局设置";
    final String basicForwardProcessCase = "基本正常流程(模型训练加载完成)";
    final String organizationRoleManageCase = "机构角色管理功能";

    @Autowired
    SaaSShCombinationApi saaSShCases;

    @Test(description = basicForwardProcessCase,dataProvider="basicForwardProcessData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void basicForwardProcessCase(TestDataBean testDataBean){

        saaSShCases.mainFlowFaction(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description = addEditDeleteOpenDiscourseCase,dataProvider="addEditDeleteOpenDiscourseData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void addEditDeleteOpenDiscourseCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description = exportOpenDiscourseCase,dataProvider="exportOpenDiscourseData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void exportOpenDiscourseCase(TestDataBean testDataBean){

        saaSShCases.exportOpenDiscourse(testDataBean,variableDataBean,variableMap);
    }

    @Test(description = batchCheckOpenDiscourseCase,dataProvider="batchCheckOpenDiscourseData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void batchCheckOpenDiscourseCase(TestDataBean testDataBean){

        saaSShCases.batchCheckOpenDiscourse(testDataBean,variableDataBean,variableMap);
    }

    @Test(description = addDeleteSeatGroupCase,dataProvider="addDeleteSeatGroupData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void addDeleteSeatGroupCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description = industryKnowledgeBaseCase,dataProvider="industryKnowledgeBaseData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void industryKnowledgeBaseCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description = customersManageModuleCase,dataProvider="customersManageModuleData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void customersManageModuleCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description = discourseTemplateModuleCase,dataProvider="discourseTemplateModuleData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void discourseTemplateModuleCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =intentionRankModuleCase,dataProvider="intentionRankModuleData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void intentionRankModuleCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =discourseDictModuleCase,dataProvider="discourseDictModuleData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void discourseDictModuleCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =knowledgeCase,dataProvider="knowledgeData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void knowledgeCase(TestDataBean testDataBean){

        saaSShCases.knowledgeBaseModule(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description =importKnowledgeCase,dataProvider="importKnowledgeData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void importKnowledgeCase(TestDataBean testDataBean){

        saaSShCases.importKnowledgeBase(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description =industryAndDiscourseClassCase,dataProvider="industryAndDiscourseClassData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void industryAndDiscourseClassCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =callStrategyManageCase,dataProvider="callStrategyManageData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void callStrategyManageCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =deleteCallStrategyCase,dataProvider="deleteCallStrategyData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void deleteCallStrategyCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =deleteMultipleCallStrategyCase,dataProvider="deleteMultipleCallStrategyData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void deleteMultipleCallStrategyCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }


    @Test(description =discourseManageCase,dataProvider="discourseManageData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void discourseManageCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =discourseFlowCase ,dataProvider="discourseFlowData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void discourseFlowCase(TestDataBean testDataBean){

        saaSShCases.flowTreeModule(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description =discourseFlowDeleteAssistFlowCase ,dataProvider="discourseFlowDeleteAssistFlowData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void discourseFlowDeleteAssistFlowCase(TestDataBean testDataBean){

        saaSShCases.flowTreeModule(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description =addDialTaskCase,dataProvider="addDialTaskData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void addDialTaskCase(TestDataBean testDataBean){

        saaSShCases.addDialTask(variableDataBean,variableMap);
    }

    @Test(description =dialPlanPartCase,dataProvider="dialPlanPartData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void dialPlanPartCase(TestDataBean testDataBean){

        saaSShCases.dialPlanPartModule(testDataBean,variableDataBean,variableMap,executeCount);
    }
    @Test(description =dialPlanPart2Case,dataProvider="dialPlanPart2Data",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void dialPlanPart2Case(TestDataBean testDataBean){

        saaSShCases.dialPlanPart2Module(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description =smsSingTemplateCase ,dataProvider="smsSingTemplateData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void smsSingTemplateCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =callRecordsCase ,dataProvider="callRecordsData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void callRecordsCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =globalSetCase ,dataProvider="globalSetData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void globalSetCase(TestDataBean testDataBean){

        saaSShCases.globalSetModule(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description =smsTaskCase ,dataProvider="smsTaskData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void smsTaskCase(TestDataBean testDataBean){

        saaSShCases.smsTaskModule(testDataBean,variableDataBean,variableMap,executeCount);
    }

    @Test(description =artificialSeatCase ,dataProvider="artificialSeatData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void artificialSeatCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =smsMonitorCase ,dataProvider="smsMonitorData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void smsMonitorCase(TestDataBean testDataBean){

        saaSShCases.execCase(testDataBean,variableDataBean,variableMap);
    }

    @Test(description =organizationRoleManageCase ,dataProvider="organizationRoleManageData",dataProviderClass = SaaSShVoiceDataProvider.class)
    public void organizationRoleManageCase(TestDataBean testDataBean){

        saaSShCases.organizationRoleManageModule(testDataBean,variableDataBean,variableMap,executeCount);
    }

}
