package com.hj.outbound.common;

import com.hj.common.dto.TestDataBean;
import com.hj.outbound.mapper.test_case_db.GetTestDataMapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 读取数据库测试数据
 */

public class ReadTestData {

    private static ReadTestData readTestData = new ReadTestData();

    GetTestDataMapper testDataMapper = SpringUtils.getBean(GetTestDataMapper.class);

    private ReadTestData() {
    }

    public static ReadTestData getInstance() {

        return readTestData;
    }

    /**
     * 读取数据库测试用例数据
     * @param caseId	用例id
     * @return
     */
    public  Iterator<Object[]> getTestDataFromDB(Integer caseId[],String useTable){

        List<Object[]> iList = new ArrayList<>();

        List<TestDataBean> geTestDataBeans = getTestData(caseId,useTable);

        for (Object object : geTestDataBeans) {
            iList.add(new Object[] { object });
        }
        return iList.iterator();
    }

    /**
     * 从数据库获取单接口的数据
     * @param caseId
     *              用例id
     * @param useTable
     *                表
     * @return
     *          TestDataBean类型引用
     */
    public TestDataBean getSingleApiData(int caseId,String useTable){

        return testDataMapper.getCases(useTable,caseId);
    }

    private List<TestDataBean> getTestData(Integer caseIds[],String useTable){

        List<TestDataBean> list = new ArrayList<>();
        TestDataBean testDataBean = null;
        for(int  caseId :caseIds){
            try {
                testDataBean = testDataMapper.getCases(useTable,caseId);
            }catch (Exception e){
                e.printStackTrace();
            }

            list.add(testDataBean);
        }
        return list;
    }
}
