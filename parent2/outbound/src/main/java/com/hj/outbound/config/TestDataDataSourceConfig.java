package com.hj.outbound.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

//xx环境数据源
@Configuration//注册到springboot容器，相当于原来xml文件里的<beans>
//下面要进行扫包，目的是标清楚为谁添加的数据源，这样对应的包里函数执行数据库操作的时候，就知道要执行的数据库账号
@MapperScan(basePackages= {"com.hj.outbound.mapper.test_case_db"},sqlSessionFactoryRef="testDataSqlSessionFactory")
public class TestDataDataSourceConfig {

    DataSourceConfigCommon dataSourceConfigCommon = new DataSourceConfigCommon();

    @Primary
    @Bean(name="testDataDataSource")
    @ConfigurationProperties(prefix="spring.datasource.testdata")
    public DataSource testDataDataSource(){

        return DataSourceBuilder.create().build();
    }
    @Primary
    @Bean(name="testDataSqlSessionFactory")
    public SqlSessionFactory testDataDataSourceFactory(@Qualifier("testDataDataSource") DataSource dataSource) throws Exception{

        return  dataSourceConfigCommon.dataSourceConfig(dataSource);
    }

}
