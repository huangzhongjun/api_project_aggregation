package com.hj.outbound.mapper.test_db;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

/**
 * 测试用例执行校验数据库数据
 */
@Qualifier("devSqlSessionFactoryRef")

public interface CaseResultCheckMapper {

    /**获取目标开放性话术数**/
    @Select("select count(1) from open_verbal where name = #{name} and status=1")
    int getOpenVerbalCount(String name);

    /**获取所有话术模板数**/
    @Select("select count(1) from call_template where is_delete=0 and org_id=2")
    int getAllDiscourseTemplateCount();
    /**获取目标话术模板数**/
    @Select("select count(1) from call_template where name = #{name} and is_delete=#{isDeleteValue}")
    int getDiscourseTemplateCount(@Param("name") String name, @Param("isDeleteValue") int isDeleteValue);
    /**获取所有已发布的话术模板数**/
    @Select("select count(1) from call_template where is_delete=0 and status=3 and org_id=2")
    int getPublishDiscourseTemplateCount();

    /**获取所有行业知识库数**/
    @Select("select count(1) from industry_knowledge where status=1 and industry_id =1")
    int getAllIndustryKnowledgeCount();
    /**获取目标行业知识库数**/
    @Select("select count(1) from industry_knowledge where name = #{name} and status=1")
    int getIndustryKnowledgeCount(String name);


    /**获取目标行业话术数**/
    @Select("select count(1) from industry_knowledge_item where name = #{name}")
    int getIndustryKnowledgeItemCount(String name);

    /**获取目标坐席组数**/
    @Select("select count(1) from agent_group where name = #{name} and status=1")
    int getAgentGroupCount(String name);

    /**获取所有客户名单数**/
    @Select("select count(1) from crm_customer where deleted=0")
    int getAllCrmCustomerCount();
    /**获取目标客户名单数**/
    @Select("select count(1) from crm_customer where customer_name = #{name} and deleted=0")
    int getCrmCustomerCount(String name);
    /**获取目标客户名单所属的批次id**/
    @Select("select crm_customer_batch_id from crm_customer where customer_name = #{name} and deleted=0")
    String getCrmCustomerBatchId(String name);

    /**获取目标客户名单所属的批次id的批次号**/
    @Select("select number from crm_customer_batch where id = #{batchId}")
    String getCrmCustomerBatchNumber(String batchId);

    /**获取目标意向等级数**/
    @Select("select count(1) from template_intention_rank where name = #{name}")
    int getTemplateIntentionRankCount(String name);

    /**获取目标客户字典数**/
    @Select("select count(1) from template_customer_dict where name = #{name} and call_template_id = #{discourseTemplateId}")
    int getCustomerDictCount(@Param("name") String name, @Param("discourseTemplateId") String discourseTemplateId);
    /**获取目标客户字典状态**/
    @Select("select status from template_customer_dict where name = #{name} and call_template_id = #{discourseTemplateId}")
    String getCustomerDictStatus(@Param("name") String name, @Param("discourseTemplateId") String discourseTemplateId);


    /**获取目标知识库(0自定义 1行业知识库 2开放话术)数**/
    @Select("select count(1) from template_knowledge where name = #{name} and is_delete=#{isDelete}")
    int getKnowledgeCount(@Param("name") String name, @Param("isDelete") int isDelete);

    /**获取所有话术分类数**/
    @Select("select count(1) from verbal_classify")
    int getAllVerbalClassifyCount();

    /**获取所有行业分类数**/
    @Select("select count(1) from verbal_industry")
    int getAllVerbalIndustryCount();

    /**获取所有拨打策略数**/
    @Select("select count(1) from call_strategy")
    int getAllCallStrategyCount();
    /**获取目标拨打策略数**/
    @Select("select count(1) from call_strategy where name = #{name}")
    int getCallStrategyCount(String name);

    /**获取目标多轮拨打策略数**/
    @Select("select count(1) from multiple_call_strategy where name = #{name}")
    int getMultipleCallStrategyCount(String name);
    /**获取多轮拨打策略表轮次配置字段值**/
    @Select("select strategy_items from multiple_call_strategy where name = #{name}")
    String getMultipleCallStrategyConfig(String name);

    /**获取所有可用线路列表数**/
    @Select("select count(1) from phone_line")
    int getAllPhoneLineCountAll();

    /**获取目标话术(自定义、开放性话术、行业知识库)数**/
    @Select("select count(1) from template_verbal_section where name = #{name} and status=1")
    int getDiscourseCount(String name);

    /**获取目标主辅助流程表目标记录数**/
    @Select("select count(1) from template_flow_group where name = #{name}")
    int getFlowGroupCount(String name);
    /**获取目标主辅助流程数**/
    @Select("select count(1) from template_flow_group where id = #{id}")
    int getFlowGroupCountFromId(String id);

    /**获取目标意图数**/
    @Select("select count(1) from template_intention where name = #{name}")
    int getIntentionCount(String name);

    /**获取目标拨打计划的id**/
    @Select("select id from call_plan where name = #{name}")
    String getCallPlanId(String name);
    /**获取目标拨打计划的呼叫状态**/
    @Select("select status from call_plan where name = #{name}")
    String getCallPlanStatus(String name);

    /**获取目标短信任务**/
    @Select("select count(1) from sms_send_batch where name = #{name}")
    int getSmsTask(String name);
    /**获取短信发送类型的短信任务数**/
    @Select("select count(1) from sms_send_batch where send_type = #{sendType}")
    int getSendTypeSmsTask(int sendType);

    /**获取NLU模型Version数据**/
    @Select("select version from template_predict_model where call_template_id = #{discourseTemplateId} limit 1")
    String getNLUVersionData(String discourseTemplateId);
    /**获取NLU模型code数据**/
    @Select("select code from template_predict_model where call_template_id = #{discourseTemplateId}")
    List<String> getNLUCodeData(String discourseTemplateId);
    /**获取NLU模型训练结果数据**/
    @Select("select status from template_predict_model where call_template_id = #{discourseTemplateId}")
    List<Integer> getNLUStatusData(String discourseTemplateId);

    /**获取流程树里的目标流程节点名数**/
    @Select("select count(1) from template_flow where name=#{name}")
    int getGoalFlowNodeData(String name);

    /**获取机构管理表里的数据-返回记录数量**/
    @Select("select count(1) from organization where name=#{name}")
    int getOrganizationTableData(String name);
    /**获取机构管理表里的数据-返回机构id**/
    @Select("select id from organization where name=#{name}")
    String getOrganizationTableIdData(String name);
    /**获取机构管理表里的数据-返回望潮机构下的所有机构名称**/
    @Select("select name from organization where superior_org_id=1")
    List<String> getOrganizationTableOrgName();

    /**查询tts发音人列表**/
    @Select("select supplier from tts_resource limit 0,1")
    String getTTSVoiceList();

    /**查询批次空号检测状态**/
    @Select("select dead_check_status from phonebook_batch where id =#{batchId}")
    String getBatchNullNumerCheckStatus(String batchId);

    /**获取角色表里的数据-返回角色id**/
    @Select("select id from role where name=#{name}")
    String getRoleTableIdData(String name);
    /**获取角色表里的数据-返回数量**/
    @Select("select count(1) from role where name=#{name}")
    int getRoleTableDataCount(String name);
    /**获取角色表里的数据-返回数量(望潮机构下的所有角色)**/
    @Select("select count(1) from role where type = 3")
    int getRoleTableDataCountAll();

    /**获取用户表里的数据-返回数量**/
    @Select("select count(1) from user where nickname=#{name}")
    int getUserTableData(String name);
    /**获取用户表里的数据-返回用户id**/
    @Select("select id from user where nickname=#{name}")
    String getUserTableIdData(String name);

    /**从表中取出一个已经发布的话术模板**/
    @Select("select id from published_call_template order by id DESC limit 1")
    String getPublishedCallTemplateId();

    /**通话记录数**/
    @Select("select count(1) from dialogue_record where phone_number=#{phoneNumber}")
    int getDialogueRecordCount(int phoneNumber);
}
