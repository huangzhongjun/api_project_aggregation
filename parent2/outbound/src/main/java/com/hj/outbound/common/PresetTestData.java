package com.hj.outbound.common;


import com.hj.common.utils.RandomUtil;
import com.hj.outbound.dto.VariableDataBean;

import java.util.Map;

/**
 * 预置测试数据
 * @author huangjun
 */

public class PresetTestData {

    public void presetTestData(VariableDataBean variableDataBean, Map<String,Object> variableMap){

        variableDataBean.setIndustryDiscourseName("自动"+ RandomUtil.randomStr(2)+"行业话术");
        variableMap.put("industryDiscourseName",variableDataBean.getIndustryDiscourseName());

        variableDataBean.setOpenDiscourseName("自动"+RandomUtil.randomStr(2)+"开放性话术");
        variableMap.put("openDiscourseName",variableDataBean.getOpenDiscourseName());

        variableDataBean.setDialPlanName("自动"+RandomUtil.randomStr(3)+"拨打计划");
        variableMap.put("dialPlanName",variableDataBean.getDialPlanName());

        variableDataBean.setMultipleDialPlanName("自动"+RandomUtil.randomStr(3)+"多轮拨打计划");
        variableMap.put("multipleDialPlanName",variableDataBean.getMultipleDialPlanName());

        variableDataBean.setIndustryKnowledgeName("自动"+RandomUtil.randomStr(2)+"行业知识库");
        variableMap.put("industryKnowledgeName",variableDataBean.getIndustryKnowledgeName());

        variableDataBean.setKnowledgeName("自动"+RandomUtil.randomStr(2)+"知识库");
        variableMap.put("knowledgeName",variableDataBean.getKnowledgeName());

        variableDataBean.setSeatGroupName("自动"+RandomUtil.randomStr(4)+"坐席组");
        variableMap.put("seatGroupName",variableDataBean.getSeatGroupName());

        customerInfo(variableDataBean,variableMap,0,"sx");

        variableDataBean.setDiscourseTemplateName("自动"+RandomUtil.randomStr(3)+"话术模板");
        variableMap.put("discourseTemplateName",variableDataBean.getDiscourseTemplateName());

        variableDataBean.setDiscourseName("自动"+RandomUtil.randomStr(3)+"话术名");
        variableMap.put("discourseName",variableDataBean.getDiscourseName());

        variableDataBean.setDiscourseContent("自动"+RandomUtil.randomStr(3)+"话术内容");
        variableMap.put("discourseContent",variableDataBean.getDiscourseContent());

        variableDataBean.setIntentionRankName("自动"+RandomUtil.randomStr(3)+"意向等级");
        variableMap.put("intentionRankName",variableDataBean.getIntentionRankName());

        variableDataBean.setCustomerDictName("自动"+RandomUtil.randomStr(2)+"客户字典");
        variableMap.put("customerDictName",variableDataBean.getCustomerDictName());

        variableDataBean.setDialStrategyName("自动"+RandomUtil.randomStr(3)+"策略");
        variableMap.put("dialStrategyName",variableDataBean.getDialStrategyName());

        variableDataBean.setMultipleDialStrategyName("自动"+RandomUtil.randomStr(2)+"多轮策略");
        variableMap.put("multipleDialStrategyName",variableDataBean.getMultipleDialStrategyName());

        variableDataBean.setAssistFlowName("自动"+RandomUtil.randomStr(3)+"辅助流程名");
        variableMap.put("assistFlowName",variableDataBean.getAssistFlowName());

        variableDataBean.setFlowNodeName("自动"+RandomUtil.randomStr(3)+"流程节点名");
        variableMap.put("flowNodeName",variableDataBean.getFlowNodeName());

        variableDataBean.setUserIntention("自动"+RandomUtil.randomStr(5)+"用户意图");
        variableMap.put("userIntention",variableDataBean.getUserIntention());

        variableDataBean.setBranchIntentionName("自动"+RandomUtil.randomStr(5)+"分支意图");
        variableMap.put("branchIntentionName",variableDataBean.getBranchIntentionName());

        variableDataBean.setSmsTemplateName("自动"+RandomUtil.randomStr(5)+"短信模板名称");
        variableMap.put("smsTemplateName",variableDataBean.getSmsTemplateName());

        variableDataBean.setSmsTaskName("自动"+RandomUtil.randomStr(5)+"短信任务名称");
        variableMap.put("smsTaskName",variableDataBean.getSmsTaskName());

        variableDataBean.setMainFlowOpenRemarks("自动主流程"+RandomUtil.randomStr(2)+"开场白");
        variableMap.put("mainFlowOpenRemarks",variableDataBean.getMainFlowOpenRemarks());

        variableDataBean.setAssistFlowOpenRemarks("自动辅助流程"+RandomUtil.randomStr(2)+"开场白");
        variableMap.put("assistFlowOpenRemarks",variableDataBean.getAssistFlowOpenRemarks());
    }

    /**客户名单-姓名、手机号、性别 参数化**/
    public Map<String,Object> customerInfo(VariableDataBean variableDataBean,Map<String,Object> variableMap,int phonesIndex,String flag){
        String phonesArray[] = {"25029","25028","25027","25026","25025","25024","25023","25022","25021"};
        variableDataBean.setCustomerName("自动"+RandomUtil.randomStr(3));
        variableMap.put("customerName",variableDataBean.getCustomerName());
        if(flag .equalsIgnoreCase("sx")){
            variableDataBean.setPhoneNum(phonesArray[phonesIndex]);
        }
        else if(flag.equalsIgnoreCase("zx")){
            variableDataBean.setPhoneNum(phonesArray[(phonesArray.length/2)]);
        }
        else if(flag.equalsIgnoreCase("dx")){
            variableDataBean.setPhoneNum(phonesArray[(phonesArray.length-1-phonesIndex)]);
        }
        variableMap.put("phoneNum",variableDataBean.getPhoneNum());

        variableDataBean.setSex(RandomUtil.randomSex());
        variableMap.put("sex",variableDataBean.getSex());
        return variableMap;
    }
}
