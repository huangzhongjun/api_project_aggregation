package com.hj.outbound.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

//91环境数据源
@Configuration//注册到springboot容器，相当于原来xml文件里的<beans>
//下面要进行扫包，目的是标清楚为谁添加的数据源，这样对应的包里函数执行数据库操作的时候，就知道要执行的数据库账号
@MapperScan(basePackages= {"com.hj.outbound.mapper.test_db"},sqlSessionFactoryRef="devSqlSessionFactoryRef")
public class DevDataSourceConfig {
    DataSourceConfigCommon dataSourceConfigCommon = new DataSourceConfigCommon();
    @Bean(name="devDataSource")
    @ConfigurationProperties(prefix="spring.datasource.dev")
    public DataSource devDataSource(){

        return DataSourceBuilder.create().build();
    }
    @Bean(name="devSqlSessionFactoryRef")
    public SqlSessionFactory devDataSourceFactory(@Qualifier("devDataSource") DataSource dataSource) throws Exception{

        return  dataSourceConfigCommon.dataSourceConfig(dataSource);
    }

}
