package com.hj.outbound.mapper.test_case_db;

import com.hj.common.dto.TestDataBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * 测试数据读取
 */

@Qualifier("testDataSqlSessionFactory")
public interface GetTestDataMapper {

    /**获取用例**/
    @Select("select * from ${tableName} where case_id = #{caseId}")
    TestDataBean getCases(@Param("tableName") String tableName, @Param("caseId") int caseId);


}
