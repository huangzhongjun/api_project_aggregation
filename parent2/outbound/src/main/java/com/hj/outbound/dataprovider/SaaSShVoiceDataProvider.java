package com.hj.outbound.dataprovider;

import com.hj.common.utils.Constant;
import com.hj.common.utils.MoreListCaseProcessUtil;
import com.hj.outbound.common.ReadTestData;
import org.testng.annotations.DataProvider;
import java.util.Iterator;


/**
 * @author huangjun
 *
 */
public class SaaSShVoiceDataProvider {

	private static String SaaSShTable = Constant.SaaSShTable;

	private static String SaaSShBasicForwardProcessTable = Constant.SaaSShFlowTreeTable;

	//////////////////////////////////////////////// 单 接 口 单 条 试 数 据 //////////////////////////////////////////////////////////
	@DataProvider
	public static Iterator<Object[]> basicForwardProcessData() {
		Integer caseIds[] = MoreListCaseProcessUtil.moreListCaseProcess(9,68);
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShBasicForwardProcessTable);
	}
	@DataProvider
	public static Iterator<Object[]> addEditDeleteOpenDiscourseData() {
		Integer caseIds[]={8,9,10,11};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> exportOpenDiscourseData() {
		Integer caseIds[]={13,158};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> batchCheckOpenDiscourseData() {
		Integer caseIds[]={12};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> addDeleteSeatGroupData() {
		Integer caseIds[]={14,16};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> industryKnowledgeBaseData() {
		Integer caseIds[]={2,3,4,5,6,7};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> customersManageModuleData() {
		Integer caseIds[]={21,22,23,24,25,26,148,149,181,182,183};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> discourseTemplateModuleData() {
		Integer caseIds[]={27,29,30,43,44,28,31,79,189};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> intentionRankModuleData() {
		Integer caseIds[]={27,32,35,36,38,34};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> discourseDictModuleData() {
		Integer caseIds[]={27,45,46,47,48,141};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> knowledgeData() {
		Integer caseIds[]={49,50,51,52,172,180,53,54,49};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> industryAndDiscourseClassData() {
		Integer caseIds[]={55,56};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> callStrategyManageData() {
		Integer caseIds[]={59,57,58,90,78,91,92,93,94,95,96};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> discourseManageData() {
		Integer caseIds[]={27,45,46,61,62,63,64,65,66,67,150,151,152,154,188};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> discourseFlowData() {
		Integer caseIds[] ={163,164,165,166,167,168,169,170};
		return  ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> discourseFlowDeleteAssistFlowData() {
		Integer caseIds[] ={171};
		return  ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> addDialTaskData() {
		Integer caseIds[]={162};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> smsSingTemplateData() {
		Integer caseIds[]={85,97,86,87,98,88,89};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}

	@DataProvider
	public static Iterator<Object[]> deleteCallStrategyData() {
		Integer caseIds[]={57,60};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> deleteMultipleCallStrategyData() {
		Integer caseIds[]={90,100};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> dialPlanPartData() {
		Integer caseIds[]={39,40,41,101,102,103,104,105};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> dialPlanPart2Data() {
		Integer caseIds[]={39,190,106,142,143,144,145,146,147};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}

	@DataProvider
	public static Iterator<Object[]> callRecordsData() {
		Integer caseIds[]={42,107,108,109,110,111,112};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> globalSetData() {
		Integer caseIds[]={113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,155};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> smsTaskData() {
		Integer caseIds[]={133,134,135,136,187,184,185,137,138,139,186,191};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> importKnowledgeData() {
		Integer caseIds[]={159,160};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> artificialSeatData() {
		Integer caseIds[]={173,174};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
	@DataProvider
	public static Iterator<Object[]> smsMonitorData() {
		Integer caseIds[]={175,176,177,178};
		return ReadTestData.getInstance().getTestDataFromDB(caseIds,SaaSShTable);
	}
    @DataProvider
    public static Iterator<Object[]> organizationRoleManageData() {
        Integer caseIds[]={2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,1,20};
        return ReadTestData.getInstance().getTestDataFromDB(caseIds,Constant.SaaSShOrgRoleTable);
    }


}
