package com.hj.outbound.service;


import com.alibaba.fastjson.JSONObject;
import com.hj.common.dto.ExecuteCount;
import com.hj.common.dto.TestDataBean;
import com.hj.common.utils.*;
import com.hj.outbound.common.PresetTestData;
import com.hj.outbound.common.ReadTestData;
import com.hj.outbound.dto.VariableDataBean;
import com.hj.outbound.mapper.test_db.CaseResultCheckMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 组合单接口形成用例场景
 */
@Component
public class SaaSShCombinationApi {

    Logger logger = LoggerFactory.getLogger(SaaSShCombinationApi.class);

    @Autowired
    SaaSShApiService saaSShApiService ;
    @Resource
    CaseResultCheckMapper caseResultCheckMapper;

    Method[] methods;

    public void execCase(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String, Object> variableMap){
        loginAndInitBean(variableDataBean,variableMap);
        methodInvoke(testDataBean);
    }

    public void execCaseDesignation(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String, Object> variableMap){
        saaSShApiService.initVariableData(variableDataBean,variableMap);
        methodInvoke(testDataBean);
    }

    private void methodInvoke(TestDataBean testDataBean){
        methods = saaSShApiService.getClass().getMethods();
        for (Method method : methods) {
            if (method.getName().equals(testDataBean.getMethodName())) {
                try {
                    method.invoke(saaSShApiService,testDataBean);
                } catch (Exception e) {
/*                    if (e.getCause() != null && e.getCause() instanceof AssertionError) {
                        throw (AssertionError)e.getCause();
                    }*/
                    throw (AssertionError)e.getCause();
                }
                break;
            }
        }
    }

    /**话术模板、短信、策略、客户字典、意向等级、知识库、短信任务(完整流程操作)**/
    private <T> void initIntactFlowBefore(VariableDataBean variableDataBean,Map<String,Object> variableMap,T executeCount){
        Integer caseId[] = {1,2,3,4,5,6,7,8,9,10,11,12,31,32,33,34,35,36,37,38,39,40,41};
        batchExecutionFrontCase(caseId, Constant.SaaSShGroup,variableDataBean,variableMap,executeCount);
    }

    /**生成一个流程树模型(未训练)**/
    private void initFlowTreeUntrained(VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        Integer[] flowCaseId = MoreListCaseProcessUtil.moreListCaseProcess(9, 63);
        batchExecutionFrontCase(flowCaseId,Constant.SaaSShFlowTreeTable,variableDataBean,variableMap,executeCount);
    }
    /**添加一键外呼**/
    private void initAddCallTask(VariableDataBean variableDataBean,Map<String,Object> variableMap,Integer addCallTaskCaseIds[],String flag){
        List<String> callTemplateIds = new ArrayList<>();//选择一个已发布的模板
        try {
            callTemplateIds.add(saaSShApiService.requestUtils.getSaveLinked().get("publishedTemplateId").toString());
        }catch (Exception e){
            logger.info("单纯的做一键外呼(单策-短信关闭)用例，发布的话术模板将从数据库随机取一个");
            callTemplateIds.add(caseResultCheckMapper.getPublishedCallTemplateId());
        }
        //调用添加一键外呼任务(单轮策略、多轮策略)
        TestDataBean addCallTaskBean;
        for (int caseId :addCallTaskCaseIds) {
            //添加选择外呼客户名单
            List<String> customerIdList = initCustomerList(variableDataBean, variableMap, "-1", 2,flag);
            addCallTaskBean = ReadTestData.getInstance().getSingleApiData(caseId, Constant.SaaSShTable);
            saaSShApiService.selectCustomerNewCallPlanApi(addCallTaskBean,callTemplateIds,customerIdList);
        }
    }
    /**添加一个行业知识库、添加若干行业话术(读取从数据库中储备好的运营使用的行业话术)**/
    private void initIndustryKnowLedgeAndIndustryDiscourse(VariableDataBean variableDataBean,Map<String,Object> variableMap,ExecuteCount executeCount){
        List<Object> list = new ArrayList<>();
        Integer industryKnowledgeCaseId[] = {};
        batchExecutionFrontCase(industryKnowledgeCaseId,Constant.SaaSShTable,variableDataBean,variableMap,executeCount);
        int industryDiscourseCaseId[] = {};
        TestDataBean industryDiscourseBean;
        for (int caseId:industryDiscourseCaseId) {
            industryDiscourseBean = ReadTestData.getInstance().getSingleApiData(caseId,Constant.SaaSShGroup);
            saaSShApiService.addIndustryDiscourseApi(industryDiscourseBean);
            list.add(saaSShApiService.requestUtils.getSaveLinked().get("industryDiscourseId"));
        }
        saaSShApiService.requestUtils.getSaveLinked().put("industryKnowLedgeIds",list);
    }
    /**添加若干开放性话术(读取从数据库中储备好的运营使用的开放性话术)**/
    private void initOpenDiscourse(){
        List<Object> list = new ArrayList<>();
        int OpenDiscourseCaseId[] = {};
        TestDataBean openDiscourseBean;
        for (int caseId:OpenDiscourseCaseId) {
            openDiscourseBean = ReadTestData.getInstance().getSingleApiData(caseId,Constant.SaaSShGroup);
            saaSShApiService.addOpenDiscourseApi(openDiscourseBean);
            list.add(saaSShApiService.requestUtils.getSaveLinked().get("openDiscourseId"));
        }
        saaSShApiService.requestUtils.getSaveLinked().put("openDiscourseIds",list);
    }

    /**获取发布的话术模板id**/
    private void getPublishDiscourseTemplateId(){
        TestDataBean publishDiscourseTemplateBean = ReadTestData.getInstance().getSingleApiData(148, Constant.SaaSShTable);
        saaSShApiService.publishedCallTemplateApi(publishDiscourseTemplateBean);
    }

    /**主流程(用例主体为 主流程树模型、拨打计划、通话记录、短信监控)**/
    public void mainFlowFaction(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        if( executeCount.getExecuteCount() == 1){
            initIntactFlowBefore(variableDataBean,variableMap,executeCount);
        }
        execCase(testDataBean,variableDataBean,variableMap);//流程树模型构建
        if(testDataBean.getApiDescription().equals("话术模板-发布")){
            //获取已发布的模板id
            getPublishDiscourseTemplateId();
            //新增两个知识库
            Integer addKnowledgeCase[] = {42,43};
            batchExecutionFrontCase(addKnowledgeCase,Constant.SaaSShGroup,variableDataBean,variableMap,executeCount);
        }
    }

    /**知识库功能模块**/
    public void knowledgeBaseModule(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        if( executeCount.getExecuteCount() == 1){
            initIntactFlowBefore(variableDataBean,variableMap,executeCount);
            initFlowTreeUntrained(variableDataBean,variableMap,executeCount);
        }
        execCase(testDataBean,variableDataBean,variableMap);
    }

    /**知识库功能模块-导入知识库(行业话术和开放性话术-在service里做知识库的判断操作)**/
    public void importKnowledgeBase(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        if( executeCount.getExecuteCount() == 1){
            Integer discourseTemplate[] = {27,61,3};//话术模板、话术、行业知识库
            batchExecutionFrontCase(discourseTemplate,Constant.SaaSShTable,variableDataBean,variableMap,executeCount);
            Integer caseIds[] = {4};//添加行业话术,拿到行业话术的id
            List<Object> list = new ArrayList<>();
            for(int count = 0; count<4 ; count++){
                variableDataBean.setIndustryDiscourseName("自动"+ RandomUtil.randomStr(2)+"行业话术");
                variableMap.put("industryDiscourseName",variableDataBean.getIndustryDiscourseName());
                batchExecutionFrontCase(caseIds,Constant.SaaSShTable,variableDataBean,variableMap,executeCount);
                list.add(saaSShApiService.requestUtils.getSaveLinked().get("industryDiscourseId"));
            }
            saaSShApiService.requestUtils.getSaveLinked().put("industryKnowLedgeIds",list);
            //添加多个开放性话术,拿到开放话术的id
            JSONObject jsonObject = initMultipleOpenDiscourse(variableDataBean, variableMap, 5);
            saaSShApiService.requestUtils.getSaveLinked().put("openDiscourseIds",jsonObject.get("ids"));
        }
        execCase(testDataBean,variableDataBean,variableMap);
    }

    /**
     * 开放性话术：批量确认
     */
    public void batchCheckOpenDiscourse(TestDataBean testDataBean,VariableDataBean variableDataBean,Map<String,Object> variableMap){
        testDataBean.setRequestData(initMultipleOpenDiscourse(variableDataBean,variableMap,5).toJSONString());
        saaSShApiService.batchCheckOpenDiscourseApi(testDataBean);
    }

    /**
     * 开放性话术：导出
     */
    public void exportOpenDiscourse(TestDataBean testDataBean,VariableDataBean variableDataBean,Map<String,Object> variableMap){
        if(testDataBean.getApiDescription().contains("单个")){
            TestDataBean singleTestDataBean = ReadTestData.getInstance().getSingleApiData(8, Constant.SaaSShTable);
            loginAndInitBean(variableDataBean,variableMap);
            saaSShApiService.addOpenDiscourseApi(singleTestDataBean);
            saaSShApiService.exportOpenDiscourseApi(testDataBean);
        }
        else {
            testDataBean.setRequestData(initMultipleOpenDiscourse(variableDataBean,variableMap,10).toJSONString());
            JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
            jsonObject.put("id","");
            jsonObject.put("name","");
            testDataBean.setRequestData(jsonObject.toJSONString());
            saaSShApiService.exportOpenDiscourseApi(testDataBean);
        }
    }

    /**
     * 添加外呼任务(单策略、sms关闭)
     * @param variableDataBean
     * @param variableMap
     */
    public void addDialTask(VariableDataBean variableDataBean,Map<String,Object> variableMap){
        initIntactFlowBefore(variableDataBean,variableMap,0);
        Integer callPlanCaseId[] = {162};
        initAddCallTask(variableDataBean,variableMap,callPlanCaseId,"zx");
    }

    /**拨打计划模块-暂停-继续-二次呼叫等操作**/
    public void dialPlanPartModule(TestDataBean testDataBean,VariableDataBean variableDataBean,Map<String,Object> variableMap,ExecuteCount executeCount){
        if(executeCount.getExecuteCount() == 2) {
            //添加两个一键外呼任务(单轮和多轮)
            Integer callDialPlanCaseId[] = {77,161};
            initAddCallTask(variableDataBean,variableMap,callDialPlanCaseId,"sx");
            executeCount.setExecuteCount(3);
        }
        execCase(testDataBean,variableDataBean,variableMap);
        if(testDataBean.getApiDescription().equals("拨打计划-查看外呼任务(单轮和多轮)")){
            executeCount.setExecuteCount(2);
        }
    }

    /**拨打计划-导出-详情-查看-下载等操作**/
    public void dialPlanPart2Module(TestDataBean testDataBean,VariableDataBean variableDataBean,Map<String,Object> variableMap,ExecuteCount executeCount){
        if(executeCount.getExecuteCount() == 2) {
            variableDataBean.setDialPlanName("自动"+RandomUtil.randomStr(3)+"拨打计划");
            variableMap.put("dialPlanName",variableDataBean.getDialPlanName());
            logger.info("开始进行【拨打计划-导出-详情-查看-下载等用例】操作啦啦啦....");
            //添加外呼任务
            Integer callPlanCaseId[] = {77};
            initAddCallTask(variableDataBean, variableMap, callPlanCaseId, "dx");
            //等待外呼任务结束
            saaSShApiService.pubCommonAction.getCallPlanStatusAlreadyStop(caseResultCheckMapper,variableDataBean.getDialPlanName(),120);
            executeCount.setExecuteCount(3);
        }
        execCase(testDataBean,variableDataBean,variableMap);
    }

    /**
     * 全局设置模块
     * @param testDataBean
     * @param variableDataBean
     * @param variableMap
     * @param executeCount
     *                     为 1 时执行前提预置操作
     *                     为 2 时不执行前提预置操作
     */
    public void globalSetModule(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        Integer caseIds[] = {27,61,14};// 先调用创建话术模板接口、创建话术接口、坐席组接口（创建节点接口-暂定）
        batchExecutionSameOperate(testDataBean,variableDataBean,variableMap,executeCount,caseIds,Constant.SaaSShTable);
    }

    /**
     * 短信任务模块
     * @param testDataBean
     * @param variableDataBean
     * @param variableMap
     * @param executeCount
     *                     为 1 时执行前提预置操作
     *                     为 2 时不执行前提预置操作
     */
    public void smsTaskModule(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        Integer caseIds[] = {85,97,87,98};//短信签名和短信模板
        batchExecutionSameOperate(testDataBean,variableDataBean,variableMap,executeCount,caseIds,Constant.SaaSShTable);
    }

    /**流程树里的列表、详情、删除用例**/
    public void flowTreeModule(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        if( executeCount.getExecuteCount() == 1){
            initIntactFlowBefore(variableDataBean,variableMap,executeCount);
            Integer[] discourseFlowCaseIds = MoreListCaseProcessUtil.moreListCaseProcess(9,63);
            batchExecutionFrontCase(discourseFlowCaseIds,Constant.SaaSShFlowTreeTable,variableDataBean,variableMap,executeCount);
        }
        execCase(testDataBean,variableDataBean,variableMap);
    }

    /**机构角色管理功能**/
    public void organizationRoleManageModule(TestDataBean testDataBean, VariableDataBean variableDataBean, Map<String,Object> variableMap, ExecuteCount executeCount){
        if(executeCount.getExecuteCount() == 1){
            Integer caseId[] = {1};
            //机构用户登录
            batchExecutionFrontCase(caseId,Constant.SaaSShOrgRoleTable,variableDataBean,variableMap,executeCount);
            //添加坐席组
            batchExecutionFrontCase(caseId,Constant.SaaSShGroup,variableDataBean,variableMap,executeCount);
        }
        execCaseDesignation(testDataBean,variableDataBean,variableMap);
        if(testDataBean.getApiDescription().equals("机构管理-删除机构")){
            saaSShApiService.requestUtils.getSaveHeadInfo().clear();
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**登录接口请求和初始化aaSShApiService类里的 variableDataBean、variableMap**/
    private void loginAndInitBean(VariableDataBean variableDataBean,Map<String,Object> variableMap){
        if(!saaSShApiService.requestUtils.getSaveHeadInfo().containsKey("Authorization")){
            saaSShApiService.loginApi();
        }
        saaSShApiService.initVariableData(variableDataBean,variableMap);
    }

    /**批量执行前置操作用例及主体用例**/
    private void batchExecutionSameOperate(TestDataBean testDataBean,VariableDataBean variableDataBean,Map<String,Object> variableMap,ExecuteCount executeCount,Integer caseIds[],String tableName){
        if( executeCount.getExecuteCount() == 1){
            batchExecutionFrontCase(caseIds,tableName,variableDataBean,variableMap,executeCount);
        }
        execCase(testDataBean,variableDataBean,variableMap);
    }

    /**
     * 批量执行前置操作的用例
     * @param caseIds
     *                  用例id（数组）
     * @param tableName
     *                  用例所在的表名
     * @param variableDataBean
     * @param variableMap
     * @param executeCount
     *                      前置操作步骤执行的次数控制
     */
    private <T> void  batchExecutionFrontCase(Integer caseIds[],String tableName,VariableDataBean variableDataBean,Map<String,Object> variableMap,T executeCount){

        try{
            Iterator<Object[]> testDataBean = ReadTestData.getInstance().getTestDataFromDB(caseIds, tableName);
            while (testDataBean.hasNext()){
                Object[] next = testDataBean.next();
                for(Object o : next){
                    execCase((TestDataBean)o,variableDataBean,variableMap);
                }
            }
        }catch (Exception e){}
        if(executeCount instanceof ExecuteCount){
            ((ExecuteCount)executeCount).setExecuteCount(2);
        }
    }

    /**
     * 生成多个开放性话术，并取得所有开放性话术的id
     * @param variableDataBean
     * @param variableMap
     * @param openDiscourseCount
     *                      初始化开放性话术的个数
     * @return
     *          JSONObject:存放着多个开放性话术的id
     */
    private JSONObject initMultipleOpenDiscourse(VariableDataBean variableDataBean,Map<String,Object> variableMap,int openDiscourseCount){

        List<Object> idsList = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        //获取添加开放性话术用例的testDataBean实体
        TestDataBean addDiscourseTestDataBean ;
        loginAndInitBean(variableDataBean,variableMap);
        //添加5个开放性话术
        for(int i = 0; i < openDiscourseCount; i++){
            addDiscourseTestDataBean = ReadTestData.getInstance().getSingleApiData(8, Constant.SaaSShTable);
            variableDataBean.setOpenDiscourseName("自动"+ RandomUtil.randomStr(2)+"开放话术");
            variableMap.put("openDiscourseName",variableDataBean.getOpenDiscourseName());
            saaSShApiService.addOpenDiscourseApi(addDiscourseTestDataBean);
            idsList.add(saaSShApiService.requestUtils.getSaveLinked().get("openDiscourseId")) ;
        }
        jsonObject.put("ids",idsList);
        return jsonObject;
    }

    /**
     * 添加多个客户名单，获取所有的客户名单Id
     * @param variableDataBean
     * @param variableMap
     * @param useCondition
     *                      添加客户外呼任务的请求参数的一个字段,0:直接勾选客户记录(也就是要传客户ids),
     *                      -1：目的是为了方便主流程操作
     * @param customerCount
     *                      客户名单数量
     * @return
     */
    private List<String> initCustomerList(VariableDataBean variableDataBean,Map<String,Object> variableMap,String useCondition,int customerCount,String flag){
        Map<String,Object> requestDataMap;
        PresetTestData presetTestData  = new PresetTestData();
        List<String> customerIds = new ArrayList<>();
        if( "0".equals(useCondition) || "-1".equals(useCondition) ) {
            //生成批次编号
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
            String batchNumber = simpleDateFormat.format(new Date());
            //以下为后面添加一键外呼的批次号(非 批次Id)查询做预置条件
            variableDataBean.setBatchNum(batchNumber);
            variableMap.put("batchNum",variableDataBean.getBatchNum());
            saaSShApiService.requestUtils.getSaveLinked().put("batchNum",batchNumber);
            //添加多个客户名单，获取所有的客户名单Id
            for (int i = 1; i <= customerCount; i++) {
                TestDataBean customerListDataBean = ReadTestData.getInstance().getSingleApiData(21,Constant.SaaSShTable);
                requestDataMap = JSONObject.parseObject(customerListDataBean.getRequestData());
                presetTestData.customerInfo(variableDataBean, variableMap,(i-1),flag);
                requestDataMap = ParamRequestData.paramRequestData(variableMap, requestDataMap);
                requestDataMap.put("customerBatchNumber",batchNumber);
                customerListDataBean.setRequestData(requestDataMap.toString());
                saaSShApiService.addDialCustomerApi(customerListDataBean);
                System.out.println("================要添加 "+customerCount+" 个客户名单,已添加"+variableDataBean.getCustomerName()+"【"+i+"】个");
                customerIds.add(saaSShApiService.requestUtils.getSaveLinked().get("customerId").toString());
            }
        }
        return  customerIds;
    }
}
