package com.hj.outbound.common;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hj.common.dto.TestDataBean;
import com.hj.common.utils.*;
import com.hj.outbound.mapper.test_db.CaseResultCheckMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 公共的辅助操作
 */
public class PubCommonAction{

    private final Logger logger = LoggerFactory.getLogger(PubCommonAction.class);

    ApiRequestUtils requestUtils;

    public PubCommonAction(ApiRequestUtils requestUtils){

        this.requestUtils = requestUtils;
    }

    /**文件流接口的处理**/
    public void fileStreamProcess(TestDataBean testDataBean){
        CloseableHttpResponse closeableHttpResponse = requestUtils.postRequestForJsonToResponse(testDataBean);
        int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
        CheckPointUtils.myAssertEquals(testDataBean.getApiDescription(),statusCode,200);
    }

    /**
     * post接口请求等,已经做了body的code和msg断言、当用例描述里包含"失败场景",则断言指定字段
     * @param testDataBean
     * @return 返回 响应body
     */
    public String postPubActionProcess(TestDataBean testDataBean, String uri) {
        testDataBean.setUrl(uri);
        String responseBody = requestUtils.postRequestForJson(testDataBean);
        if(testDataBean.getApiDescription().contains("异常场景")){
            Map<String,Object> checkMap = (Map)JSONObject.parseObject(testDataBean.getChecks());
            CheckPointUtils.checkPoints(responseBody,checkMap,testDataBean.getApiDescription());
        }else{
            CheckPointUtils.checkResponseCodeMsg(responseBody, testDataBean.getApiDescription());
        }
        return responseBody;
    }

    /**
     * get接口请求等,已经做了body的code和msg断言
     * @param testDataBean
     *                  请求参数的父节点、接口地址的key
     */
    public String getPubActionProcess(TestDataBean testDataBean,String uri){
        testDataBean.setUrl(uri);
        // 接口请求
        String responseBody = requestUtils.getRequest(testDataBean);
        CheckPointUtils.checkResponseCodeMsg(responseBody,testDataBean.getApiDescription());
        return responseBody;
    }

    /**
     * delete接口请求等,已经做了body的code和msg断言
     * @param testDataBean
     *                  请求参数的父节点、接口地址的key
     */
    public String deletePubActionProcess(TestDataBean testDataBean,String uri){
        testDataBean.setUrl(uri);
        // 接口请求
        String responseBody = requestUtils.deleteRequest(testDataBean);
        CheckPointUtils.checkResponseCodeMsg(responseBody,testDataBean.getApiDescription());
        return responseBody;
    }

    /**
     * 请求参数-参数化变量
     */
    public void requestParamInitialize(TestDataBean testDataBean,Map<String,Object> variableMap){
        Map<String,Object> requestMap = (Map<String,Object>) JSONObject.parse(testDataBean.getRequestData());
        requestMap = ParamRequestData.paramRequestData(variableMap, requestMap);
        testDataBean.setRequestData(requestMap.toString());
    }

    /**
     * 请求数据 [检查点]-参数化变量
     */
    public Map<String,Object> requestDataCheckInitialize(TestDataBean testDataBean,Map<String,Object> variableMap){
        Map<String,Object> checkMap = JSONObject.parseObject(testDataBean.getChecks());
        if(null ==checkMap){
            CheckPointUtils.myAssertFalse("【"+testDataBean.getApiDescription()+"】接口 检查点数据不存在或格式有问题");
        }
        checkMap = ParamRequestData.paramRequestData(variableMap, checkMap);
        testDataBean.setChecks(checkMap.toString());
        return checkMap;
    }

    /**
     * [请求参数] 和 [请求数据检查点]-参数化变量
     */
    public Map<String,Object>  requestDataInitialize(TestDataBean testDataBean,Map<String,Object> variableMap){
        requestParamInitialize(testDataBean,variableMap);
        return requestDataCheckInitialize(testDataBean,variableMap);
    }

    /**
     * 数据库查询【校验】某张表的所有记录和和响应body的记录数
     * @param allCountDB
     *                  数据库表总记录数
     * @param bodyCount
     *                  响应body里的记录数
     * @param apiName
     *                 接口名称
     */
    public void checkPointFromDBQueryAll(int allCountDB,int bodyCount,String apiName) {
        if (allCountDB == bodyCount) {
            logger.info("校验数据库表记录数和响应body里的记录数一致");
        } else {
            CheckPointUtils.myAssertFalse("【"+apiName+"接口】数据库表记录数和响应body里的记录数不一致：数据库记录数：" + allCountDB + "响应body记录数：" + bodyCount);
        }
    }


    /**
     * 数据库查询【校验】某张表的新增记录是否入库
     * @param goalCountDB
     *                  从数据库中获取到的记录数
     * @param apiName
     *                  接口名
     */
    public void checkPointFromDBQueryGoal(int goalCountDB,String apiName) {
        if (goalCountDB == 1) {
            logger.info("校验数据库表记录数和响应body里的记录数一致,入库了");
        } else {
            CheckPointUtils.myAssertFalse("【"+apiName+"接口】操作- 数据库表记录数和响应body里的记录数不一致,入库有问题,sql返回数量是 "+goalCountDB+" 条,期望是1条");
        }
    }

    /**
     * 数据库查询【校验】某张表的异常用例执行后，数据是否入库
     * @param goalCountDB
     *                  从数据库中获取到的记录数
     * @param apiName
     *                  接口名
     */
    public void checkPointFromDBQueryForExceptionCase(int goalCountDB,String apiName) {
        if (goalCountDB == 0) {
            logger.info("校验数据库表记录数和响应body里的记录数一致,异常用例的数据未入库");
        } else {
            CheckPointUtils.myAssertFalse("【"+apiName+"接口】操作- 数据库表记录数和响应body里的记录数不一致,入库有问题,sql返回数量是 "+goalCountDB+" 条,期望是0条");
        }
    }

    /**
     * 数据库查询【校验】某张表的删除的是否成功
     * @param goalCountDB
     *                  从数据库中获取到的记录数
     * @param apiName
     *                  接口名
     */
    public void checkPointFromDBQueryGoalIsDelete(int goalCountDB,String apiName) {
        if (goalCountDB == 0) {
            logger.info("【"+apiName+"】接口数据库校验成功，数据库的目标数据已经不在了");
        } else {
            CheckPointUtils.myAssertFalse("【"+apiName+"】接口数据库校验失败，数据库的目标数据还存在");
        }
    }

    /**
     * 处理json中一个corpus的值是json字符串，而不是一个json
     * @param testDataBean
     */
    public void jsonStringAsValue(TestDataBean testDataBean){
        //corpus字段的json串值处理
        JSONObject jsonObject = JSONObject.parseObject(testDataBean.getRequestData());
        JSONArray jsonArray = jsonObject.getJSONArray("corpus");
        jsonObject.put("corpus",jsonArray.toString());
        testDataBean.setRequestData(jsonObject.toString());
    }

    /**
     * 分页断言
     * @param responseBody
     *                  响应body
     * @param apiName
     *                  接口名称
     * @param checkPoints
     *                  检测点
     * @param flag
     *
     */
    public static void paginationCheck(String responseBody,String apiName,String checkPoints[],int flag,int openVerbalCountDB){

        String totalJP = checkPoints[1];
        String sizeJP = checkPoints[2];
        String currentJP = checkPoints[3];
        String pagesJP = checkPoints[4];

        int actualTotal = Integer.parseInt(JsonPathRead.readJson(responseBody, totalJP, apiName));
        int actualSize = Integer.parseInt(JsonPathRead.readJson(responseBody, sizeJP, apiName));
        int actualCurrent = Integer.parseInt(JsonPathRead.readJson(responseBody, currentJP, apiName));
        int actualPages = Integer.parseInt(JsonPathRead.readJson(responseBody, pagesJP,apiName));

        CheckPointUtils.myAssertEquals(apiName, actualSize, 20);//每页数
        CheckPointUtils.myAssertEquals(apiName, actualCurrent, 1);//当前页

        if ( 0 == flag) {
            CheckPointUtils.myAssertEquals(apiName, actualTotal, openVerbalCountDB);//总记录数
            CheckPointUtils.myAssertEquals(apiName, actualPages, 1);//有多少页
        }
        else {
            CheckPointUtils.myAssertEquals(apiName, actualTotal, openVerbalCountDB);//总记录数
            //计算总共多少页
            int pages = openVerbalCountDB/20;
            if(openVerbalCountDB % 20 != 0){
              pages+=1;
            }
            CheckPointUtils.myAssertEquals(apiName, actualPages, pages);//总页数
        }
    }

    /**
     * 获取拨打计划的状态
     * @param statusCode
     *                  状态码
     * @return
     *          状态信息
     */
    public String getCallPlanStatus(int statusCode){

        switch (statusCode){
            case 2: return "等待执行";
            case 3: return "调度中";
            case 4: return "执行中";
            case 5: return "暂停中";
            case 6: return "已暂停";
            case 9: return "已完成";
            case 11: return "失败";
            default:return "没有匹配到状态值";
        }
    }

    /**
     * 获取拨打计划状态为已暂停
     * @param caseResultCheckMapper
     * @param dialPlanName
     * @return
     */
    public int  getCallPlanStatusAlreadyStop(CaseResultCheckMapper caseResultCheckMapper, String dialPlanName, int time){
        int count = 0;
        int callPlanStatusDB;
        do{
            callPlanStatusDB = Integer.parseInt(caseResultCheckMapper.getCallPlanStatus(dialPlanName));
            logger.info("外呼任务的当前状态为："+getCallPlanStatus(callPlanStatusDB));
            if(count > time){
                CheckPointUtils.myAssertFalse("继续呼叫接口前提是状态是已暂停,等待"+time+"秒,呼叫状态仍然是:"+getCallPlanStatus(callPlanStatusDB));
            }
            SleepUtil.sleep(0.5);
            count ++;
        }
        while (callPlanStatusDB !=6);
        return callPlanStatusDB;
    }

    /**
     * 继续呼叫后，外呼任务的状态变更检测
     */
    public void continueCallCheckStatus(CaseResultCheckMapper caseResultCheckMapper,String dialPlanName){
        int callPlanStatusDB;
        int executeCount=0;//执行状态的次数
        int count = 0;
        //执行->已暂停(呼叫完成)
        do{
            callPlanStatusDB = Integer.parseInt(caseResultCheckMapper.getCallPlanStatus(dialPlanName));
            if(callPlanStatusDB == 4 ){ //执行中
                ++executeCount;
                logger.info("拨打计划的拨打状态是 [执行中] 了......");
            }
            else if(callPlanStatusDB ==6){
                if( executeCount == 0) {
                    CheckPointUtils.myAssertFalse("继续呼叫接口调用后,经过2分钟了，呼叫状态没有经过执行中,从调度中到已暂停");
                }
                else {
                    logger.info("外呼任务呼叫完成！！");
                    break;
                }
            }
            else {
                logger.info("外呼任务当前状态是："+getCallPlanStatus(callPlanStatusDB));
            }
            if(count ==120){
                CheckPointUtils.myAssertFalse("继续呼叫接口调用后,经过2分钟了，外呼任务当前状态是："+getCallPlanStatus(callPlanStatusDB));
            }
            SleepUtil.sleep();
            count++;
        }while (count<121);
    }

    /**暂停呼叫任务**/
    public void stopCallTask(TestDataBean testDataBean,CaseResultCheckMapper caseResultCheckMapper,String dialPlanName,String apiUrl){
        int callPlanStatusDB;
        for(int i = 0; i< 40 ;i++){
            callPlanStatusDB = Integer.parseInt(caseResultCheckMapper.getCallPlanStatus(dialPlanName));
            logger.info("等待呼叫状态为执行中/已暂停.....当前状态是:"+getCallPlanStatus(callPlanStatusDB));
            if(callPlanStatusDB == 4 || callPlanStatusDB ==6){ //执行中或已暂停
                logger.info("拨打计划的拨打状态是 [执行中、已暂停] 了,准备进行暂停操作...............");
                if(callPlanStatusDB ==6){
                    logger.info("拨打计划的拨打状态是 [已暂停] 了，不能进行暂停接口操作了...............");
                }
                else {
                    postPubActionProcess(testDataBean,apiUrl);
                }
                break;
            }
            if(callPlanStatusDB == 5){//暂停中
                SleepUtil.sleep();
                continue;
            }
            SleepUtil.sleep();
            if(callPlanStatusDB == 11){
                CheckPointUtils.myAssertFalse("外呼任务调度失败...........");
            }
            if(i == 39 && callPlanStatusDB == 3){
                CheckPointUtils.myAssertFalse("外呼任务经过40秒了,状态还是调度中.....");
            }
            if(i == 39 && callPlanStatusDB == 5){
                CheckPointUtils.myAssertFalse("外呼任务经过40秒了,状态还是暂停中.....");
            }
        }
    }

}
